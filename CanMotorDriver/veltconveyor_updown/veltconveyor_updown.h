#ifndef VELTCONVEYOR_H
#define VELTCONVEYOR_H

#include <Arduino.h>
#include <motor.h>
#include <mcp_can.h>

// 各種設定
static const uint8_t UPDOWN_MOTOR  = MOTOR1;
static const uint8_t LIMIT_TOP     = INPUT1_1;
static const uint8_t LIMIT_BOTTOM  = INPUT1_2;
static const uint8_t CAN_SPEED     = CAN_250KBPS;
static const uint8_t TPIP_CAN_ADDR = 1;
static const uint8_t self_can_addr = 7;

#define LIMIT_TOP_PUSHED (digitalRead(LIMIT_TOP) == HIGH)
#define LIMIT_BOTTOM_PUSHED (digitalRead(LIMIT_BOTTOM) == HIGH)

extern int8_t motor_up_dir[2];

// -255 ~ 255, 0 : stop(not free)
extern void table_up(int16_t speed);
extern void check_limit();

extern void set_motor_dir(const int8_t *dirs);

#define CAN_COMMAND_TABLE_UP 1

#endif
