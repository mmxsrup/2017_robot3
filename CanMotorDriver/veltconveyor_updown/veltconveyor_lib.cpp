#include "veltconveyor_updown.h"


// 1で前進相当、-1で後退相当になるよう設定
int8_t table_motor_up_dir = 1;

// -255 ~ 255, 0 : stop(not free)
void table_up(int16_t speed) {
    Serial.println("Up");
    //motorWrite(UPDOWN_MOTOR, speed * table_motor_up_dir);

    if(speed > 0 && !LIMIT_TOP_PUSHED) {
        motorWrite(UPDOWN_MOTOR, speed * table_motor_up_dir);
    } else if(speed < 0 && !LIMIT_BOTTOM_PUSHED) {
        motorWrite(UPDOWN_MOTOR, speed * table_motor_up_dir);
    } else {
        motorStop(UPDOWN_MOTOR);
        Serial.println("motor stop");
        if(LIMIT_TOP_PUSHED || LIMIT_BOTTOM_PUSHED) Serial.println("ERROR : LIM SW NG");
    }
}

void check_limit() {
    if(LIMIT_TOP_PUSHED
       && motorGetMode(UPDOWN_MOTOR) * table_motor_up_dir == MOTOR_MODE_CCW) { // speed < 0
        motorStop(UPDOWN_MOTOR);
    }
    if(LIMIT_BOTTOM_PUSHED
       && motorGetMode(UPDOWN_MOTOR) * table_motor_up_dir == MOTOR_MODE_CW) { // speed < 0
        motorStop(UPDOWN_MOTOR);
    }
}
