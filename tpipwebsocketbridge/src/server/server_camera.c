/*
  server_camera.c

  JPEG通信用プロトコルの実装

  受け入れるデータ

  {
      get : true, これを受け取ると現在の各種設定値を格納したjsonを返す
      streaming   : bool, カメラ画像のストリーミングをするかどうか設定する（初期値ON）
      camera : int , ストリーミングするカメラの番号を選択する(通常 0, 1, 2)
      size : "VGA" or "QVGA" , ストリーミングの解像度を設定する
      kbps : int TPIPからJPEG画像を送る速度を設定する kbps 単位
  }
 
  送るデータ

  jpeg形式のバイナリデータ または 以下の json
 
  {
      ip : string , 接続（or接続試行）中のTPIP ip
      camera : int , カメラの番号
      kbps : int TPIPからのJPEG画像送信速度（実測値） kbps 単位
      status : string ,接続状態を表す文字列(現状 "Connected" | "Not Connected")
  }
 
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "jansson.h"
#include "tpip.h"

#include "server_camera.h"
#include "private.h"

//
//
// private declearation
//
//

// struct

struct per_session_data__camera {
    int streaming_duration_ms;     // 送る周期の最大値
    bool request_streaming;        // 画像をストリーミングするかどうか
    bool request_status;      // 現在の設定を送る時用のflg
    time_t last_streamed_time;
};

// function

static int callback_camera_stream(struct lws *wsi,
                                  enum lws_callback_reasons reason,
                                  void *user,
                                  void *in,
                                  size_t len);

static bool update_user_setting(json_t *root,
                                struct lws *wsi,
                                struct per_session_data__camera *user);
static json_t *build_status_json();

// variable

static struct {
    struct lws_context *context;
    int user_num;
    lws_send_buffer send_jpeg_buf;
    lws_send_buffer send_setting_buf;
} local_;


//
//
// public
//
//

struct lws_protocols server_protcol_camera = {
        "camera-stream",
        callback_camera_stream,
        sizeof(struct per_session_data__camera),
        100000,
};


bool server_init_camera(struct lws_context *context) {
    if(context == NULL) return false;
    local_.context = context;
    local_.user_num = 0;
    return true;
}

bool server_tasks_camera() {
    if(local_.context == NULL) return false;
    if(local_.user_num == 0) return false;
    
    int len;
    void* ptr = tpip_get_jpeg(&len);
    if(ptr == NULL) return false;
    
    lws_send_buffer_set(&local_.send_jpeg_buf, ptr, len);
    tpip_free_jpeg();
    lws_callback_on_writable_all_protocol(local_.context,
                                          &server_protcol_camera);
    return true;
}


extern bool server_destroy_camera() {
    local_.context = NULL;
    return true;
}

//
//
// private
//
//

// コールバック関数群

static int on_connection(struct lws *wsi,
                         struct per_session_data__camera *user_data) {
    
    user_data->streaming_duration_ms = 33;
    user_data->request_streaming = true;
    user_data->request_status = false;
    user_data->last_streamed_time = clock();

    if(local_.user_num == 0) tpip_connect_jpeg();
    local_.user_num += 1;
    
    return 0;
}

static int on_writable(struct lws *wsi,
                       struct per_session_data__camera *user_data) {
    
    if(user_data->request_status) {
        json_t *data = build_status_json();
        char *data_str = json_dumps(data, JSON_COMPACT);
        lws_send_buffer_set(&local_.send_setting_buf,
                            data_str, strlen(data_str));
        lws_send_buffer_send(&local_.send_setting_buf,
                             wsi, LWS_WRITE_TEXT);
        free(data_str);
        json_decref(data);
        user_data->request_status = false;
    }
    else if(user_data->request_streaming) {
        time_t now = clock();
        time_t prev = user_data->last_streamed_time;
        int duration = user_data->streaming_duration_ms;
        
        if((double)(now - prev) / CLOCKS_PER_SEC * 1000 >= duration) {
            lws_send_buffer_send(&local_.send_jpeg_buf,
                                 wsi, LWS_WRITE_BINARY);
            user_data->last_streamed_time = now;
        }
    }
    return 0;
}

static int on_receive(struct lws *wsi,
                      struct per_session_data__camera *user_data,
                      const char* in_str) {
    json_error_t err;
    json_t *root = json_loads(in_str, 0, &err);
    if(!root) {
        fprintf(stderr, "%s error: on line %d: %s\n",
                __func__, err.line, err.text);
        return -1; // reject & close connection
    }
    if(!update_user_setting(root, wsi, user_data)){
        return -1;
    }
    
    json_decref(root);
    return 0;
}

static int on_close(struct lws *wsi,
                    struct per_session_data__camera *user_data) {
    local_.user_num -= 1;
    if(local_.user_num == 0) {
        tpip_close_jpeg();
    }
    return 0;
}


//
//
// 補助関数
//
//


 static bool update_user_setting(json_t *root,
                                 struct lws *wsi,
                                 struct per_session_data__camera *user) {
     
     if(!json_is_object(root)) return false;
     
     bool request_status = false;
     
     const char *key;
     json_t *value;
     json_object_foreach(root, key, value) {
         if(strcmp(key, "get") == 0) {
             request_status = true;
         }
         else if(strcmp(key, "streaming") == 0) {
             if(json_is_boolean(value)) {
                 user->request_streaming = json_boolean_value(value);
             }
         }
         else if(strcmp(key, "camera") == 0) {
             if(json_is_integer(value)) {
                 tpip_set_camera_no((int)json_integer_value(value));
             }
         }
         else if(strcmp(key, "size") == 0) {
             if(json_is_string(value)) {
                 const char *size = json_string_value(value);
                 tpip_set_jpeg_size( strcmp(size, "QVGA") == 0 ? 0 :
                                     strcmp(size, "VGA") == 0  ? 1 : 0);
             }
         }
         else if(strcmp(key, "kbps") == 0) {
             if(json_is_integer(value)) {
                 json_int_t kbps = json_integer_value(value);
                 tpip_set_jpeg_request_speed(kbps);
             }
         }
         else if(strcmp(key, "fps") == 0) {
             puts("FPS");
             if(json_is_integer(value)) {
                 json_int_t fps = json_integer_value(value);
                 user->streaming_duration_ms = 1.0 / fps * 1000;
                 printf("set camera fps %d\n", (int)fps);
             }
         }
     }
     if(request_status) {
         user->request_status = true;
         lws_callback_on_writable(wsi);
     }
     return true;
 }

static json_t *build_status_json() {
    json_t *root = json_object();
    json_object_set_new(root, "ip",
                        json_string(tpip_get_target_ip()));
    json_object_set_new(root, "camera",
                        json_integer(tpip_get_camera_no()));
    json_object_set_new(root, "kbps",
                        json_integer(tpip_get_jpeg_actual_speed()));
    json_object_set_new(root, "status",
                        json_string(tpip_is_jpeg_connected() ?
                                    "Connected" : "Not Connected"));
    return root;
}




int callback_camera_stream(struct lws *wsi, enum lws_callback_reasons reason,
                          void *user, void *in, size_t len) {
    switch (reason) {
        case LWS_CALLBACK_ESTABLISHED:
            lwsl_notice("Connected client to camera stream server.\n");
            on_connection(wsi, (struct per_session_data__camera*)user);
            break;
        case LWS_CALLBACK_CLOSED:
            lwsl_notice("Disconnected client from camera stream server.\n");
            on_close(wsi, (struct per_session_data__camera*)user);
            break;
        case LWS_CALLBACK_SERVER_WRITEABLE:
            on_writable(wsi, (struct per_session_data__camera*)user);
            break;
        case LWS_CALLBACK_RECEIVE:
            on_receive(wsi, (struct per_session_data__camera*)user, (const char*)in);
            break;
        case LWS_CALLBACK_FILTER_PROTOCOL_CONNECTION:
            dump_handshake_info(wsi);
            break;
    }
    return 0;
}
