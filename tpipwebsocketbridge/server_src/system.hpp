#ifndef SYSTEM_HPP
#define SYSTEM_HPP

#include <stdint.h>
#include <cstring>
#include <cassert>
#include <map>
#include <iostream>

#include "jansson.h"
#include "tpip.h"
#include "lib/can.h"
#include "lib/buffer.hpp"

/*
 * ボード定義
 */

enum class CAN_ID : uint8_t {
    BROAD_CAST = 0 ,
    TPIP ,
    WHEEL_MASTER,
    WHEEL_SLAVE = 3,
    BELT_HORI = 6,
	BELT_VERT = 7,
    RESCUE_BOARD,
    ARM_MOTOR,
    ARM_SERVO,
    CAN_BOARD_NUM
};

static_assert((int)CAN_ID::CAN_BOARD_NUM <= 15, "CAN BOARD ID should be less than 15");



/*
 * メカナムコマンド
 *
  for メカナム
  mecanum : {
      stop : true.
      free : true,
      roll : {
          dir : 正 or 0 or 負,
          speed : 0 to 255
      },
      go : {
          dir : -180 to 180,
          speed : 0 to 255
      }
  }
  go, roll, stop, free どれかのみ、複数含まれる場合は上の表の上から優先
  値が範囲外の場合はreject

  @ret : TPIP_ERROR or TPIP_OK
 */

enum class CAN_COMMANDS_MECANUM : uint8_t {
    GO = 1,
    ROLL,
    STOP,
    FREE
};


extern bool parse_mecanum(json_t *json);

/*
 * ベッドコンベアコマンド
 *
  for bed
  beltconveyor : {
  go : -255 to 255
  roll : -255 to 255
  }

*/

enum class CAN_COMMANDS_BELT : uint8_t {
    AUTO_CONTROL = 0,
    GO,
    ROLL
};

extern bool parse_belt_hori(json_t *json);

enum class CAN_COMMANDS_TABLE : uint8_t {
	UP = 1
};


extern bool parse_belt_vert(json_t *json);

/*
 * レスキューボードコマンド
 */
 

enum class CAN_COMMANDS_RESCUE_BOARD : uint8_t {
    GO = 1,
    ROLL
};


enum class CAN_COMMANDS_ARM_MOTOR : uint8_t {
	MOTOR = 1
};

enum class CAN_COMMANDS_SERVO : uint8_t {
	RESET = 0,
	ARM = 1,
	CAMERA = 2
};

extern bool parse_arm(json_t *json);

extern SendBuffer<(uint8_t)CAN_ID::TPIP> can_send_buffer;

#endif
