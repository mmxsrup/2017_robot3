/**
* file   : arm.c
* date   : 2016-06-26
*/

#include <stdio.h>
#include <time.h>
#include <math.h>

#include "lib/can.h"

#include "system.hpp"

static int8_t top, middle, bottom;

bool parse_arm(json_t *json) {
	json_t *motor = json_object_get(json, "motor");
	if (json_is_object(motor)) {
		json_t *speed_json = json_object_get(motor, "speed");
		if (json_is_number(speed_json)) {
			double speed = json_number_value(speed_json);
			if (speed > 127) speed = 127;
			if (speed < -127) speed = -127;
			can_send_buffer.update(
				(uint8_t)CAN_ID::ARM_MOTOR,
				(uint8_t)CAN_COMMANDS_ARM_MOTOR::MOTOR, (int8_t)speed);
		}
	}
	json_t *servo = json_object_get(json, "servo");
	if (json_is_object(servo)) {
		json_t *top_j = json_object_get(servo, "top");
		json_t *middle_j = json_object_get(servo, "middle");
		json_t *bottom_j = json_object_get(servo, "bottom");

		auto normalize = [](double a) -> int8_t {
			if (a > 90) return 90;
			else if (a < -90) return -90;
			else return a;
		};

		if (json_is_number(top_j)) top = normalize(json_number_value(top_j));
		if (json_is_number(middle_j)) middle = normalize(json_number_value(middle_j));
		if (json_is_number(bottom_j)) bottom = normalize(json_number_value(bottom_j));

		can_send_buffer.update(
			(uint8_t)CAN_ID::ARM_SERVO,
			(uint8_t)CAN_COMMANDS_SERVO::ARM,
			top, middle, bottom);
	}

	json_t *reset = json_object_get(json, "reset");
	if (json_is_boolean(reset)) {
		uint8_t off = json_boolean_value(reset);
		can_send_buffer.update(
			(uint8_t)CAN_ID::ARM_SERVO,
			(uint8_t)CAN_COMMANDS_SERVO::RESET, off);
	}
	return false;
}