#include <stdio.h>

#include "lib/can.h"

#include "system.hpp"


static bool parse_go(json_t *json);
static bool parse_roll(json_t *json);
static bool parse_stop(json_t *json);
static bool parse_free(json_t *json);

//
//
// public
//
//

bool parse_mecanum(json_t *json) {
	if (parse_stop(json) || parse_free(json) ||
		parse_roll(json) || parse_go(json)) {
		return TPIP_OK;
	}
	return TPIP_ERROR;
}


//
//
// private
//
//

static bool parse_go(json_t *json) {
	json_t *obj = json_object_get(json, "go");
	if (json_is_object(obj)) {
		json_t *dir_json = json_object_get(obj, "dir");
		json_t *speed_json = json_object_get(obj, "speed");

		if (json_is_number(dir_json) && json_is_number(speed_json)) {
			int16_t dir;
			uint8_t speed;
			{
				double dir_d = json_number_value(dir_json);
				double speed_d = json_number_value(speed_json);
				if (dir_d > INT16_MAX || INT16_MIN > dir_d) return false;
				if (speed_d > UINT8_MAX) speed_d = UINT8_MAX;
				if (speed_d < 0) speed_d = 0;
				dir = dir_d;
				speed = speed_d;
			}

			dir = ((dir % 360) + 360) % 360;
			if (dir > 180) dir -= 360;

			can_send_buffer.update(
				(uint8_t)CAN_ID::WHEEL_MASTER,
				(uint8_t)CAN_COMMANDS_MECANUM::GO,
				(int16_t)dir,
				(uint8_t)speed);

			return true;
		}
	}
	return false;
}

static bool parse_roll(json_t *json) {
	json_t *obj = json_object_get(json, "roll");
	if (json_is_object(obj)) {
		json_t *dir_json = json_object_get(obj, "dir");
		json_t *speed_json = json_object_get(obj, "speed");
		if (json_is_number(dir_json) && json_is_number(speed_json)) {
			int8_t dir;
			uint8_t speed;
			{
				double dir_d = json_number_value(dir_json);
				double speed_d = json_number_value(speed_json);
				if (speed_d > UINT8_MAX || 0 > speed_d) return false;
				dir = (dir_d == 0 ? 0 : (dir_d > 0 ? 1 : -1));
				speed = speed_d;
			}

			can_send_buffer.update(
				(uint8_t)CAN_ID::WHEEL_MASTER,
				(uint8_t)CAN_COMMANDS_MECANUM::ROLL,
				(int8_t)dir,
				(uint8_t)speed);
			return true;
		}
	}
	return false;
}

static bool parse_stop(json_t *json) {
	json_t *obj = json_object_get(json, "stop");
	if (json_is_true(obj)) {
		can_send_buffer.update(
			(uint8_t)CAN_ID::WHEEL_MASTER,
			(uint8_t)CAN_COMMANDS_MECANUM::STOP);
		return true;
	}
	return false;
}

static bool parse_free(json_t *json) {
	json_t *obj = json_object_get(json, "free");
	if (json_is_true(obj)) {
		can_send_buffer.update(
			(uint8_t)CAN_ID::WHEEL_MASTER,
			(uint8_t)CAN_COMMANDS_MECANUM::FREE);
		return true;
	}
	return false;
}

