#include <stdio.h>
#include <time.h>
#include <math.h>

#include "lib/can.h"

#include "system.hpp"

bool parse_belt_hori(json_t *belt_hori) {
	if (json_is_object(belt_hori)) {
		json_t *go = json_object_get(belt_hori, "go");
		json_t *roll = json_object_get(belt_hori, "roll");
		if (json_is_number(go)) {
			double speed = json_number_value(go);
			if (speed > 255) speed = 255;
			if (speed < -255) speed = -255;
			can_send_buffer.update(
				(uint8_t)CAN_ID::BELT_HORI,
				(uint8_t)CAN_COMMANDS_BELT::GO,
				(int16_t)speed);
		}
		if (json_is_number(roll)) {
			double speed = json_number_value(roll);
			if (speed > 255) speed = 255;
			if (speed < -255) speed = -255;
			can_send_buffer.update(
				(uint8_t)CAN_ID::BELT_HORI,
				(uint8_t)CAN_COMMANDS_BELT::ROLL,
				(int16_t)speed);
		}
		return true;
	}
	return false;
}

bool parse_belt_vert(json_t *belt_vert) {
	if (json_is_object(belt_vert)) {
		json_t *up = json_object_get(belt_vert, "up");
		if (json_is_number(up)) {
			double speed = json_number_value(up);
			if (speed > 255) speed = 255;
			if (speed < -255) speed = -255;
			can_send_buffer.update(
				(uint8_t)CAN_ID::BELT_VERT,
				(uint8_t)CAN_COMMANDS_TABLE::UP,
				(int16_t)speed);
		}
		return true;
	}
	return false;
}