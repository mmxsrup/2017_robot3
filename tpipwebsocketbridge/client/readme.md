# クライアントのサンプルプログラム

## ブラウザ版

- FireFoxで動作確認した

- サーバーを起動してブラウザでhttp://localhost:8080を開くと動く

詳細は各htmlに書いたつもり

- can-test.html が分量としては最小のサンプル

- camera, sound あたりが内容・分量的に有用

- control-test.htmlはサーバーの動作確認用

- mecanum はデモ要素が強い

---

## python3版

- 2016年2号機でマスタースレーブを作った時に用意したサンプル
- Arduino と連携する

使い方

1. Arduinoスケッチが client/python/arduino に入っているのでArduinoに書き込む
2. client/python/client.py を実行