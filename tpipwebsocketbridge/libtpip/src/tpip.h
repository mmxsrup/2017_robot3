/**
 * @file tpip.h
 * @brief TPJT / TPLX , TPIP2 / TPIP3 のラッパーライブラリ
 * @author Kenya Ukai
 * @date 2016/04/29
 */
 
 /*
    メモ :
    true -> 正常 / false -> 異常
 
  */
 
#ifndef TPIP_H
#define TPIP_H

#include <stdint.h>
#include <stdbool.h>
#include "tpip_config.h"
#include "tpip_hardware.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 *
 * 定義
 *
 */
    
#define TPIP_OK (true)
#define TPIP_ERROR (false)
    
#define TPIP_JPEG_QVGA (0)
#define TPIP_JPEG_VGA (1)

#define TPIP_SOUND_SAMPLE_RATE_8000 (8000)
#define TPIP_SOUND_SAMPLE_RATE_16000 (16000)
#define TPIP_SOUND_SAMPLE_RATE_32000 (32000)
//#define TPIP_SOUND_FORMAT_S8      (0x8008)
//#define TPIP_SOUND_FORMAT_U8      (0x0008)
//#define TPIP_SOUND_FORMAT_S8      (0x8008)
//#define TPIP_SOUND_FORMAT_U16LSB  (0x0010)
#define TPIP_SOUND_FORMAT_S16LSB  (0x8010) // TPIPはこれのよう
//#define TPIP_SOUND_FORMAT_U16MSB  (0x1010)
//#define TPIP_SOUND_FORMAT_S16MSB  (0x9010)
//#define TPIP_SOUND_FORMAT_U16     TPIP_SOUND_FORMAT_U16LSB
#define TPIP_SOUND_FORMAT_S16     TPIP_SOUND_FORMAT_S16LSB
#define TPIP_SOUND_FORMAT_DEFAULT TPIP_SOUND_FORMAT_S16LSB
#define TPIP_SOUND_FORMAT_F32LSB    0x8120
#define TPIP_SOUND_FORMAT_F32MSB    0x9120
#define TPIP_SOUND_FORMAT_F32       TPIP_SOUND_FORMAT_F32LSB
    
/*
 *
 *  構造体
 *
 */
 
struct tpip_control_data {
    bool digital[TPIP_DIGITAL_OUT_COUNT];
    int16_t pwm[TPIP_PWM_OUT_COUNT];
    int16_t motor[TPIP_MOTOR_COUNT];
};

struct tpip_sensor_data {
    bool digital[TPIP_DIGITAL_IN_COUNT];   // high : true, low : false
    uint16_t analog[TPIP_ANALOG_IN_COUNT]; // 12bit精度 0 to 65535 , 0 ~ 5V
    int16_t pulse[TPIP_PULSE_IN_COUNT];  
    uint16_t battery;                       // 0 ~ 3000, [V*10]
};

struct tpip_can_data {
    uint16_t standard_id;
    uint8_t size;
    uint8_t data[8];
};

struct tpip_sound_data {
    int32_t sample_rate;
    int32_t channel_num;
    uint16_t format; // SDLに従う.下のようなフォーマット
    /*
      +----------------------sample is signed if set
      |
      |        +----------sample is bigendian if set
      |        |
      |        |           +--sample is float if set
      |        |           |
      |        |           |  +--sample bit size---+
      |        |           |  |                    |
      15 14 13 12 11 10  9  8  7  6  5  4  3  2  1  0
     */
    uint8_t *data;
    int size; // 音声データbyte
    int allocated; // data配列の確保済みbyte数
};

/*
 *
 *
 * 関数
 *
 */

/*
 * 一般
 */

extern bool tpip_init(const char* ip);
extern bool tpip_is_inited();
extern bool tpip_close();

// 無線LAN強度 0 ~ 100 (%)
extern int tpip_get_wlink_quality();
// 制御ボードと正常に通信できているかどうか
extern bool tpip_is_controlboard_working();
// 接続中のTPIPのアドレス
extern const char* tpip_get_target_ip();    
    
/*
 * 制御
 */
    
extern bool tpip_connect_control();
extern bool tpip_close_control();
// リモート接続系なら接続できているかどうか、内部系なら初期化に成功しているかどうか
// true ならば制御可能状態、false なら制御不可状態を示す、Jpeg通信は関係ない
extern bool tpip_is_control_connected();

extern bool tpip_get_sensor(struct tpip_sensor_data *data);
extern bool tpip_set_control(const struct tpip_control_data *data);
extern bool tpip_get_control(struct tpip_control_data *data);

/*
 * 映像
 */

extern bool tpip_connect_jpeg();
extern bool tpip_close_jpeg();
// Jpeg通信非対応ライブラリでは常に false
extern bool tpip_is_jpeg_connected();
    
extern void* tpip_get_jpeg(int *size);
extern bool tpip_free_jpeg();
    
// jpeg通信の速度設定
extern bool tpip_set_jpeg_request_speed(int speed_kbps);
extern int tpip_get_jpeg_request_speed();
// kbps 、実際に出てている速度を取得
extern int tpip_get_jpeg_actual_speed();
    
// 画像サイズ TPIP_JPEG_QVGA or VGA
extern bool tpip_set_jpeg_size(int intype);
// (要求 -> 画像データ受信の)遅延時間ms
extern int tpip_get_jpeg_delay_time();
extern bool tpip_set_camera_no(int no);
extern int tpip_get_camera_no();

/*
 * CAN通信 
 */

/// @retval 受信済みのパケット数(max 1の実装あり)
extern int tpip_is_can_available();
extern bool tpip_receive_can(struct tpip_can_data *data);
extern bool tpip_send_can(const struct tpip_can_data *data);

/*
 * 音声
 */

// sample_rate E {8000, 16000, 32000}, channel_num E {1, 2}, buffer_size E [1, 20]
extern bool tpip_connect_sound(const char *ip, int sample_rate, int channel_num, int buf_size);
extern bool tpip_close_sound();
extern bool tpip_is_sound_in_connected();
extern bool tpip_is_sound_out_connected();

extern bool tpip_receive_sound(struct tpip_sound_data *data);
extern bool tpip_clear_sound_in_buffer();
extern bool tpip_send_sound(const struct tpip_sound_data *data);
extern int tpip_get_sound_sample_rate();
extern int tpip_get_sound_channel_num();
extern int tpip_get_sound_in_volume();
extern int tpip_get_sound_out_volume();
extern int tpip_get_sound_received_size();

extern bool tpip_set_sound_in_volume(int volume); // -100 ~ 100
extern bool tpip_set_sound_out_volume(int volume); // -100 ~ 100

extern bool tpip_sound_data_reserve(struct tpip_sound_data *data, int size); // 既に確保した量が指定より大きかったら何もしない
extern bool tpip_sound_data_shrink_to_fit(struct tpip_sound_data *data, int size); // vector shrink_to_fit の催事明示的に指定バージョン
extern bool tpip_sound_data_free(struct tpip_sound_data *data);

extern bool tpip_sound_data_convert_int16_to_float32(struct tpip_sound_data *dest,
                                                     const struct tpip_sound_data *src);
bool tpip_sound_data_convert_float32_to_int16(struct tpip_sound_data *dest,
                                              const struct tpip_sound_data *src);

/*
 * シリアル通信
 */
extern bool tpip_connect_serial(const char *ip, int port);
extern bool tpip_close_serial();
extern bool tpip_send_serial(const void *buf, int size);
extern bool tpip_receive_serial(void *buf, int buf_size, int *size);
    
#ifdef __cplusplus
}
#endif

#endif
