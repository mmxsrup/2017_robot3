/**
 * file   : tpsi.cpp
 * date   : 2016-06-23
 */

#include <stdbool.h>
#include "tpip.h"

#ifdef TPIP3
#include "tpip3.h"
#elif defined TPIP2
#include "tpip2.h"
#endif
#include "tpip_private.h"

namespace {

struct {
    bool is_opened;
    w32udp *udp;
} g_;

}

bool tpip_connect_serial(const char *ip, int port) {
    if(g_.is_opened) return TPIP_ERROR;
    else {
        g_.udp = new w32udp("UDP_C");
#ifdef TPJT2
        if( g_.udp->open((char*)ip, port, 500) == 0) { // エラーでなくても0が帰ってくる様子, TPIP2だとタイムアウトの挙動が怪しい
#else
		if( g_.udp->open((char*)ip, port, -1) == 0) {
#endif
/*
            g_.udp->close();
            delete g_.udp;
            return TPIP_ERROR;
*/
        }

        g_.is_opened = true;
    }
	return TPIP_OK;
}

bool tpip_close_serial() {
    if(g_.is_opened) {
        g_.udp->close();
        delete g_.udp;
        return TPIP_OK;
    } else TPIP_OK;
}
bool tpip_send_serial(const void *buf, int size) {
    if(g_.udp->send((void*)buf, size) == 0) return TPIP_ERROR;
}
bool tpip_receive_serial(void *buf, int buf_size, int *size) {
    *size = g_.udp->recv(buf, buf_size);
	//*size = g_.udp->recv_rt(buf, buf_size, -1);
    return *size > 0 ? TPIP_OK : TPIP_ERROR;
}

