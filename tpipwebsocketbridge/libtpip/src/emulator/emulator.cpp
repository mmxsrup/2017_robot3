#include <vector>
#include <random>
#include <string.h>

#include "tpip.h"
#include "tpip_private.h"

#ifdef TPIP_EMULATOR_USE_OPENCV
#include <opencv2/opencv.hpp>
#endif

namespace {
#ifdef TPIP_EMULATOR_USE_OPENCV
cv::VideoCapture video_cap_;
std::vector<uchar> img_buf_;
#endif
std::mt19937 mt_random;
char ip_[16];
bool is_inited_;
int camera_no_;
int jpeg_speed_;
tpip_control_data control_data_;
}
/*
 * 制御・一般
 */ 

bool tpip_init(const char* ip) {
    if(is_inited_) return strcmp(ip_, ip) == 0 ? TPIP_OK : TPIP_ERROR;
    std::random_device seed_gen;
    mt_random = std::mt19937(seed_gen());
    strncpy(ip_, ip, sizeof(ip_) - 1);
    is_inited_ = true;
    return TPIP_OK;
}

bool tpip_close() {
    if(!is_inited_) return TPIP_ERROR;
    is_inited_ = false;
    return TPIP_OK;
}

bool tpip_is_inited() {
    return is_inited_;
}

int tpip_get_wlink_quality() {
    return mt_random() % 101;
}

bool tpip_is_controlboard_working() {
    return true;    
}

const char* tpip_get_target_ip() {
    return ip_;
}

/*
 * 制御
 */

bool tpip_connect_control() {
    return TPIP_OK;
}

bool tpip_close_control() {    
    return TPIP_OK;
}

// リモート接続系なら接続できているかどうか、内部系なら初期化に成功しているかどうか
// true ならば制御可能状態、false なら制御不可状態を示す、Jpeg通信は関係ない
bool tpip_is_control_connected() {
    return true;
}

bool tpip_get_sensor(struct tpip_sensor_data *data) {
    for(int i = 0; i < TPIP_ANALOG_IN_COUNT; i++) {
        data->analog[i] = mt_random() % (TPIP_ANALOG_IN_MAX - TPIP_ANALOG_IN_MIN) - TPIP_ANALOG_IN_MIN;
    }
    data->battery = mt_random() % (TPIP_BATTERY_MAX - TPIP_BATTERY_MIN) - TPIP_BATTERY_MIN;
    return TPIP_OK;
}

bool tpip_set_control(const struct tpip_control_data *data) {
    control_data_ = *data;
    return TPIP_OK;
}

bool tpip_get_control(struct tpip_control_data *data) {
    *data = control_data_;
    return true;
}

/*
 * 映像
 */

bool tpip_connect_jpeg() {
#ifdef TPIP_EMULATOR_USE_OPENCV
    if(video_cap_.open(0)) {
        std::cerr << "camera 0 opened" << std::endl;
    } else if(video_cap_.open("./video.mp4")){
        std::cerr << "./video.mp4 opened" << std::endl;
    } else {
        std::cerr << "open media failed..." << std::endl;
        return TPIP_ERROR;
    }
#endif
    return TPIP_OK;
}

bool tpip_close_jpeg() {
#ifdef TPIP_EMULATOR_USE_OPENCV
    if(video_cap_.isOpened()) video_cap_.release();
#endif
    return TPIP_OK;
}

bool tpip_is_jpeg_connected() {
#ifdef TPIP_EMULATOR_USE_OPENCV
    return video_cap_.isOpened();
#endif
    return true;
}

void* tpip_get_jpeg(int *size) {
#ifdef TPIP_EMULATOR_USE_OPENCV
    cv::Mat frame;
    video_cap_ >> frame;
    if(frame.empty()) return NULL;
    else {
        std::vector<int> param(2);
        param[0] = CV_IMWRITE_JPEG_QUALITY;
        param[1] = 30;
        cv::Mat dest;
        cv::resize(frame, dest, cv::Size(640, 480));
        imencode(".jpg", dest, img_buf_, param);
        *size = img_buf_.size();
        return &img_buf_[0];
    }    
#endif    
    return NULL;
}

bool tpip_free_jpeg() {
    return TPIP_OK;
}

bool tpip_set_jpeg_request_speed(int speed_kbps) {
    jpeg_speed_ = speed_kbps;
    return TPIP_OK;
}

int tpip_get_jpeg_request_speed() {
    return jpeg_speed_;
}

int tpip_get_jpeg_actual_speed() {
    return jpeg_speed_ + mt_random() % 100 - 50;
}
// 画像サイズ TPIP_JPEG_QVGA or VGA
bool tpip_set_jpeg_size(int size_type) {
    return TPIP_OK;    
}

// (要求 -> 画像データ受信の)遅延時間ms
int tpip_get_jpeg_delay_time() {
    return 0;
}

bool tpip_set_camera_no(int no) {
    camera_no_ = no;
    return TPIP_OK;
}

int tpip_get_camera_no() {
    return camera_no_;
}

/*
 * CAN通信 
 */

/// @retval 受信済みのパケット数(max 1の実装あり)
int tpip_is_can_available() {
    return TPIP_OK;
}

bool tpip_receive_can(struct tpip_can_data *data) {
    return TPIP_OK;
}

bool tpip_send_can(const struct tpip_can_data *data) {
    return TPIP_OK;
}

/*
 * シリアル通信
 */
bool tpip_connect_serial(const char *ip, int port) {
    return TPIP_OK;
}
bool tpip_close_serial() {
    return TPIP_OK;
}
bool tpip_send_serial(const void *buf, int size) {
    std::cerr << "Serial : ";
    for(int i = 0; i < size; i++) std::cerr << ((const char*)buf)[i];
    std::cerr << std::endl;
}

bool tpip_receive_serial(void *buf, int buf_size, int *size) {
    *size = 0;
    return TPIP_OK;
}
