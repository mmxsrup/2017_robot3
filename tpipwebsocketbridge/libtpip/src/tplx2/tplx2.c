/**
 * file   : tplx2.c
 * date   : 2016-06-22
 */

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>


#include "tpip.h"
#include "tpip_private.h"
#include "TPLX.h"

static struct {
    bool is_inited;
    bool control_connection;
    struct tpip_control_data control_data;
    bool can_received;
    tpip_can can_received_data;
} g_;

bool tpip_init(const char* ip) {
    if(g_.is_inited) return TPIP_OK;
    if(TPLX_init() < 0) {
        fprintf(stderr, "TPIP init failed.\n");
        return TPIP_ERROR;
    }
    if(TPLX_set_dir(1) < 0) {
        fprintf(stderr, "TPIP set can dir failed.\n");
        return TPIP_ERROR;
    }
    g_.is_inited = true;
    return TPIP_OK;
}

bool tpip_is_inited() {
    return g_.is_inited;
}

bool tpip_close() {
    if(!g_.is_inited) return TPIP_ERROR;
    if(TPLX_close() < 0) return TPIP_ERROR;
    g_.is_inited = false;
    return TPIP_OK;
}

int tpip_get_wlink_quality() {
    return -1;
}

bool tpip_is_controlboard_working() {
    return true;
}

const char* tpip_get_target_ip() {
    return "";
}

/*
 * 制御
 */

bool tpip_connect_control() {
    tpip_ctrl ctl;
    ctl.e_bit = ENABLE_ALL;
    if(TPLX_set_ctrl(&ctl) == -1) return TPIP_ERROR;
    g_.control_connection = true;
    return TPIP_OK;
}

bool tpip_close_control() {
    tpip_ctrl ctl;
    ctl.e_bit = 0;
    if(TPLX_set_ctrl(&ctl) == -1) return TPIP_ERROR;
    g_.control_connection = false;
    return TPIP_OK;
}

// リモート接続系なら接続できているかどうか、内部系なら初期化に成功しているかどうか
// true ならば制御可能状態、false なら制御不可状態を示す、Jpeg通信は関係ない
bool tpip_is_control_connected() {
    return g_.control_connection;
}

bool tpip_get_sensor(struct tpip_sensor_data *data) {
    tpip_sens sens;
    if(TPLX_get_sens(&sens) < 0) return TPIP_ERROR;

    for(int i = 0; i < TPIP_DIGITAL_IN_COUNT; i++){
        data->digital[i] = (sens.DI & (1 << i)) ? true : false;
    }
    for(int i = 0; i < TPIP_ANALOG_IN_COUNT; i++){
        data->analog[i] = sens.AI[i];
    }
    data->pulse[0] = sens.PI[0];
    data->pulse[1] = sens.PI[1];
    data->battery = sens.PWR;
    return TPIP_OK;
}

bool tpip_set_control(const struct tpip_control_data *data) {
    memcpy(&g_.control_data, data, sizeof(g_.control_data));
    tpip_ctrl ctl = { 0 };
    ctl.e_bit = ENABLE_ALL;
    for(int i = 0; i < TPIP_DIGITAL_OUT_COUNT; i++){
        ctl.DO.value |= ((data->digital[i] ? 1 : 0) << i);
    }
    for(int i = 0; i < TPIP_PWM_OUT_COUNT; i++){
        ctl.PWM[i] = data->pwm[i];
    }
    ctl.DC = data->motor[0];
    return TPLX_set_ctrl(&ctl) == 0 ? TPIP_OK : TPIP_ERROR;
}

bool tpip_get_control(struct tpip_control_data *data) {
    *data = g_.control_data;
    return TPIP_OK;
}

/*
 * CAN通信 
 */

/// @retval 受信済みのパケット数(max 1の実装あり)
int tpip_is_can_available() {
    if(g_.can_received) return 1;
    else {
        g_.can_received_data.flg = 0;
        g_.can_received_data.RTR = 0;
        g_.can_received_data.stat = 0;
        if(TPLX_get_can(&g_.can_received_data, 0) == -1) {
            // fprintf(stderr, "TPLX_get_can failed.\n");
            return 0;
        }
        if(g_.can_received_data.flg) g_.can_received = true;
        return g_.can_received ? 1 : 0;
    }
}

bool tpip_receive_can(struct tpip_can_data *data) {
    if(data == NULL) return TPIP_ERROR;
    if(g_.can_received) {
        data->standard_id = g_.can_received_data.STD_ID;
        data->size = TPIP_MIN(8, g_.can_received_data.sz);
        memcpy(data->data, g_.can_received_data.data, data->size);
        g_.can_received = false;
        return TPIP_OK;
    } else {
        if(tpip_is_can_available()) return tpip_receive_can(data);
        else return TPIP_ERROR;
    }
}

bool tpip_send_can(const struct tpip_can_data *data) {
    if(data == NULL) return TPIP_ERROR;
    tpip_can snd_data;
    snd_data.flg = 1;
    snd_data.RTR = 0;
    snd_data.sz = TPIP_MIN(data->size, 8);
    snd_data.stat = 0;
    snd_data.STD_ID = data->standard_id;
    memcpy(snd_data.data, data->data, snd_data.sz);
    //printf("send can ");
    //printf("len : %d \n", snd_data.sz);
    //printf("id : %d \n", snd_data.STD_ID);
    //for(int i = 0; i < snd_data.sz; i++) printf("%c ", snd_data.data[i]);
    //printf("\n");
    int res = TPLX_send_can(&snd_data);
    //printf("res : %d\n", res);
    return res == 0 ? TPIP_OK : TPIP_ERROR;
}

/*
 * 映像（ダミー実装）
 */

bool tpip_connect_jpeg() { return TPIP_OK; }
bool tpip_close_jpeg() { return TPIP_OK; }
bool tpip_is_jpeg_connected() { return false; }
void *tpip_get_jpeg(int *size) { *size = 0; return NULL; }
bool tpip_free_jpeg() { return TPIP_OK; }
bool tpip_set_jpeg_request_speed(int speed_kbps) { return TPIP_OK; }
int tpip_get_jpeg_request_speed() { return 0; }
int tpip_get_jpeg_actual_speed() { return 0; }
bool tpip_set_jpeg_size(int size_type) { return TPIP_OK; }
int tpip_get_jpeg_delay_time() { return 0; }
bool tpip_set_camera_no(int no) { return TPIP_OK; }
int tpip_get_camera_no() { return 0; }

/*
 * 音声（ダミー実装）
 */

bool tpip_connect_sound(const char *ip, int sample_rate, int channel_num,
                        int buf_size) {
    return TPIP_OK;
}
bool tpip_close_sound() { return TPIP_OK; }
bool tpip_is_sound_in_connected() { return false; }
bool tpip_is_sound_out_connected() { return false; }
bool tpip_receive_sound(struct tpip_sound_data *data) { return TPIP_ERROR; }
bool tpip_clear_sound_in_buffer() { return TPIP_OK; }
bool tpip_send_sound(const struct tpip_sound_data *data) { return TPIP_ERROR; }
int tpip_get_sound_sample_rate() { return 32000; }
int tpip_get_sound_channel_num() { return 1; }
int tpip_get_sound_in_volume() { return 1; }
int tpip_get_sound_out_volume() { return 1; }
int tpip_get_sound_received_size() { return 0; }
bool tpip_set_sound_in_volume(int volume) { return TPIP_OK; }
bool tpip_set_sound_out_volume(int volume) { return TPIP_OK; }

/*
 * シリアル通信（ダミー）
 */
bool tpip_connect_serial(const char *ip, int port) { return TPIP_OK; }
bool tpip_close_serial() { return TPIP_OK; }
bool tpip_send_serial(const void *buf, int size) { return TPIP_ERROR; }
bool tpip_receive_serial(void *buf, int buf_size, int *size) {
    *size = 0;
    return TPIP_ERROR;
}
