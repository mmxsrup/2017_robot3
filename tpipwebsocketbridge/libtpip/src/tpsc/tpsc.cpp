#include <cstring>
#include "tpip.h"
#ifdef TPIP3
#include "tpip3.h"
#elif defined TPIP2
#include "tpip2.h"
#endif
#include "tpip_private.h"

namespace {
char ip_[16];
bool is_inited_;
int in_amp_;
int out_amp_;
}

bool tpip_connect_sound(const char *ip, int sample_rate, int channel_num, int buf_size) {
    if(is_inited_) return strcmp(ip_, ip) == 0 ? TPIP_OK : TPIP_ERROR;
	strncpy(ip_, ip, sizeof(ip_) - 1);
    if(TPSC_open(ip_, sample_rate, channel_num, buf_size) < 0) return TPIP_ERROR;
    TPSC_set_sndin_amp(1);
    TPSC_clear_sndin_bf();
    is_inited_ = true;
    return TPIP_OK;
}

bool tpip_close_sound() {
    if(!is_inited_) return TPIP_ERROR;
    is_inited_ = false;
    return TPIP_OK;
}

bool tpip_is_sound_in_connected() {
    return (TPSC_get_status() & (1 << 0) ? TPIP_OK : TPIP_ERROR);
}

bool tpip_is_sound_out_connected() {
    return (TPSC_get_status() & (1 << 1) ? TPIP_OK : TPIP_ERROR);
}


bool tpip_receive_sound(struct tpip_sound_data *data) {
    tpip_sound_data_reserve(data, tpip_get_sound_received_size());
    data->size = TPSC_get_data((short*)data->data, data->allocated);
	data->format = TPIP_SOUND_FORMAT_S16;
    return data->size >= 0 ? TPIP_OK : TPIP_ERROR;
}

bool tpip_clear_sound_in_buffer() {
    TPSC_clear_sndin_bf();
    return TPIP_OK;
}

bool tpip_send_sound(const struct tpip_sound_data *data) {
    // TODO データ形式の変換
    if(data->sample_rate != tpip_get_sound_sample_rate()
       || data->channel_num != tpip_get_sound_channel_num()
       || data->format != TPIP_SOUND_FORMAT_DEFAULT) return TPIP_ERROR;
    // TODO : バッファ空き待ちの処理
    // if(TPSC_wait_sndout_rsize(data->data_size, 0) >= data->data_size) -> OK
    return TPSC_put_data((short*)data->data, data->size);
}

int tpip_get_sound_sample_rate() {
    return TPSC_get_sample();
}

int tpip_get_sound_channel_num() {
    return TPSC_get_ch();
}

int tpip_get_sound_in_volume() {
    return in_amp_;
}

int tpip_get_sound_out_volume() {
    return out_amp_;
}

int tpip_get_sound_received_size() {
    return TPSC_get_sndin_size();
}

bool tpip_set_sound_in_volume(int volume) {
    in_amp_ = TPSC_set_sndin_amp(volume);
    return TPIP_OK;
}

bool tpip_set_sound_out_volume(int volume) {
    // TODO : -1 エラーって何?    
    out_amp_ = TPSC_set_sndout_amp(volume);
    return TPIP_OK;
}
