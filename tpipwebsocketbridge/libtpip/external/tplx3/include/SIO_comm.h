﻿/** 
 * @file  SIO_comm.h
 * @brief RS232 SIO commnication API for Linux header file
 *
 * @author Katayama
 * @date 2015-01-29
 * @version ver 1.00 2015/01/29 katayama [Renewal from sioDrv.h]
 *
 * Copyright (C) 2015 TPIP User Community All rights reserved.
 * このファイルの著作権は、TPIPユーザーコミュニティの規約に従い
 * 使用許諾をします。
 */

#ifndef __SIO_COMM_H__
#define __SIO_COMM_H__

extern int  SIO_init( char *ComNM, int BaudRate, int DataBits, int Parity, int StopBits, int TimeOut );
extern int  SIO_close( int fd );
extern int  SIO_recvChar( int fd, int timeout );
extern int  SIO_recv( int fd, void *buffer, int Size, int timeout );
extern int  SIO_send( int fd, void *buffer, int Size);


#define NOPARITY            0
#define ODDPARITY           1
#define EVENPARITY          2

#define ONESTOPBIT          0
#define ONE5STOPBITS        1
#define TWOSTOPBITS         2

#define SETRTS              0x02
#define CLRRTS              0x00

#endif // #ifndef __SIO_COMM_H__

