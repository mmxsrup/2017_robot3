/** 
 * TPLX3 - TPIP3 Linux Control Library Public Header
 * @file    TPLX3.h
 * @brief   TPIP3 LINUX版制御ライブラリ 公開用ヘッダファイル
 *
 * @author  Katayama
 * @date    2014-09-09
 * @version 1.00 2014/09/09
 * @version 1.10 2015/02/25 katayama
 * @version 1.11 2016/03/16 katayama
 *
 * Copyright (C) 2014 TPIP User Community All rights reserved.
 * このファイルの著作権は、TPIPユーザーコミュニティの規約に従い
 * 使用許諾をします。
 */

#ifndef __TPLX3_H__
#define __TPLX3_H__

#pragma pack(1)

// 制御出力データ構造体
typedef struct OUT_DT_STR {
	unsigned short d_out;		// DO bit3 - 0
	short          nouse1;		// 
	short          PWM[4];		// PWM ch1-4
	short          PWM2[16];	// PWM ch5-20
} out_dt_t;

// センサ入力データ構造体 
typedef struct INP_DT_STR {
	unsigned short b_ptn;		//
	unsigned short AI[8];		// AI ch1-8
	short          PI[4];		// PI ch1-4
	unsigned short batt;		// Battery monitor(0 - 3000)
	short          PI1;			// PI[0]と同じ
	short          PI2;			// PI[1]と同じ
	unsigned short DI;			// bit3 - 0
} in_dt_t;

// JTPC ボディ部 ヘッダー定義
typedef struct mctrl_hd_str{
	unsigned char  ver;		// Header Version & F/W version
	unsigned char  msg_no;		// Message No
	unsigned char  d_id;		// Data ID
	unsigned char  stat;		// no use & F/W status
} mctrl_hd_t ;


// General データフレーム定義
typedef struct {
	mctrl_hd_t  hd;
	char        dt[36];
} mctrl_dt_t;


// CAN data 
typedef struct mctrl_can_str{	// Ver 2.00a
	unsigned char  flg;		// send/receive flag
	unsigned char  RTR;		// RTR
	unsigned char  sz;		// data size
	unsigned char  stat;	// status
	unsigned short STD_ID;	// standard ID
	unsigned char  data[8];	// data
} mctrl_can_t ;

// I2C　データ 
typedef struct mctrl_i2c_str{
	unsigned char  slave_id;	// スレーブID
	unsigned char  nouse;		// no use
	unsigned int   sz;			// data size
	unsigned char  data[1024];	// data
} mctrl_i2c_t ;


#pragma pack(8)

extern void  TPLX_init(char* host_ip, void* hwnd);
extern void  TPLX_init_ex( char* host_ip, void *hwnd, int req );
extern void  TPLX_close(void);


/**
 * @brief 制御出力データ送信関数
 * @fn    void TPLX_set_ctrl( void* buf, int sz )
 * @param buf  : 制御出力データの先頭アドレス
 * @param sz   : 制御出力データのサイズ
 */

extern void  TPLX_set_ctrl(void* buf, int sz);

/**
 * @brief センサ入力データ受信関数
 * @fn    void TPLX_get_sens( void* buf, int buf_sz, int wait_tm )
 * @param buf     : センサデータの格納アドレス
 * @param buf_sz  : センサデータの格納エリアのサイズ
 * @param wait_tm : 待ち時間(ms)
 */

extern void  TPLX_get_sens(void* buf, int buf_sz, int wait_tm);

/**
 * @brief  制御ボードステータス取出関数
 * @fn     int TPLX_Get_stat( int no )
 * @param  no   : 制御基盤NO（0,1,2,3）
 * @retval bit0 : パラメータ設定完了
 * @retval bit3 : RS-232受信有り
 * @retval bit6 : RS-232通信正常
 * @retval bit7 : ファームウェア正常
 */

extern int   TPLX_Get_stat(int no);

/**
 * @brief  通信Ver取出関数
 * @fn     int TPLX_get_com_ver( void )
 * @retval >  0  : バージョン番号
 * @retval == 0  : 未接続
 */

extern int TPLX_get_com_ver( void );
 
/**
 * @brief  通信状態ID取出関数
 * @fn     int TPLX_get_com_mode( void )
 * @retval >  0  : 通信状態ID
 * @retval == 0  : 未接続
 */

extern int TPLX_get_com_mode( void );
 
/**
 * @brief  通信モード（結果）取出関数
 * @fn     int TPLX_get_com_stat( void )
 * @retval >  0  : 通信モード（結果）
 * @retval == 0  : 未接続
 */

extern int TPLX_get_com_stat( void );

/**
 * @brief  無線LAN電波強度取出関数
 * @fn     int TPLX_Get_Wlink( void )
 * @retval >  0  : 電波強度値
 * @retval == 0  : 未接続
 */

extern int   TPLX_Get_Wlink(void);

/**
 * @brief  通信遅延時間取出関数
 * @fn     int TPLX_Get_dtime( void )
 * @retval 通信遅延時間[ms]
 */

extern int   TPLX_Get_dtime(void);

/**
 * @brief  通信滞留時間取出関数
 * @fn     TPLX_Get_Staytime( void )
 * @retval 通信滞留時間[ms]
 */

extern int   TPLX_Get_Staytime(void);

/**
 * @brief  CANデータ送信関数
 * @fn     int TPLX_Send_CANdata( int no, mctrl_can_t* buf, int sz )
 * @param  no    : 制御基盤NO（0,1,2,3）
 * @param  buf   : CAN送信データバッファアドレス
 * @param  sz    : CAN送信データサイズ
 * @retval >  0  : 正常終了
 * @retval == 0  : エラー
 */

extern int TPLX_Send_CANdata( int no, mctrl_can_t* buf, int sz );
 
/**
 * @brief  CANデータ受信関数
 * @fn     int TPLX_Recv_CANdata( int no, mctrl_can_t* buf )
 * @param  no    : 制御基盤NO（0,1,2,3）
 * @param  buf   : CAN受信データ格納バッファアドレス
 * @retval >  0  : 受信有り（受信サイズ）
 * @retval == 0  : 受信無し
 */

extern int TPLX_Recv_CANdata( int no, mctrl_can_t* buf );
 
/**
 * @brief  I2Cデータ送信関数
 * @fn     int TPLX_Send_I2Cdata( int no, char* buf, int slave_id, int sz )
 * @param  no        : 制御基盤NO（0,1,2,3）
 * @param  buf       : I2C送信データバッファアドレス
 * @param  slave_id  : I2C送信先スレーブID
 * @param  sz        : I2C送信データサイズ
 * @retval >  0      : 正常終了
 * @retval == 0      : エラー
 */

extern int TPLX_Send_I2Cdata( int no, char* buf, int slave_id, int sz );
 
/**
 * @brief  I2C受信リクエスト関数
 * @fn     int TPLX_Req_Recv_I2Cdata( int no, int slave_id, int sz )
 * @param  no        : 制御基盤NO（0,1,2,3）
 * @param  slave_id  : I2C送信先スレーブID
 * @param  sz        : I2C受信データサイズ（1～32）
 * @retval >  0      : 正常終了
 * @retval == 0      : エラー
 */

extern int TPLX_Req_Recv_I2Cdata( int no, int slave_id, int sz );
 
/**
 * @brief  I2Cデータ受信関数
 * @fn     int TPLX_Recv_I2Cdata( int no, mctrl_i2c_t* buf )
 * @param  no    : 制御基盤NO（0,1,2,3）
 * @param  buf   : I2C受信データ格納バッファアドレス
 * @retval >  0  : 正常終了
 * @retval == 0  : エラー
 */

extern int TPLX_Recv_I2Cdata( int no, mctrl_i2c_t* buf );

#endif /* ifndef __TPLX3_H__ */
