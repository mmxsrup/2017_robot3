/** 
 * @file rLOG_sv.h
 * @brief rLOG server header file
 *
 * @author  Katayama
 * @date    2014-10-10
 * @version 1.00 2014/10/10
 * @version 1.01 2015/07/27 katayama
 * @version 1.02 2015/07/28 katayama
 *
 * Copyright (C) 2014 TPIP User Community All rights reserved.
 * このファイルの著作権は、TPIPユーザーコミュニティの規約に従い
 * 使用許諾をします。
 */
#ifndef __RLOG_SV_H__
#define __RLOG_SV_H__

#include <stdio.h>
#include <string.h>

extern int rLOG_init(void);
extern int rLOG_end(void);
extern int rLOG_put(char* msg, int size);

extern int rLOG_recv(void);
extern int rLOG_get_stat(void);
extern unsigned long rLOG_get_time(void);
extern void rLOG_reset_time(void);

static char log_msg[256];
#define _LOG_PRINT(format, ...) {sprintf(log_msg, format, __VA_ARGS__); rLOG_put(log_msg, strlen(log_msg));}
#define _LOG_TIME (rLOG_get_time())

#endif  // #ifndef __RLOG_SV_H__

