/**
 * @file    TPLX.h
 * @brief   TPIP Linux Lib.
 * @author  Iwata Naoki(Sanritz Automation Co.,Ltd.)
 * @date    2011/06/10
 * @version 1.00  2010/06/30 Iwata Naoki
 * @version 1.01  2010/08/03 Iwata Naoki
 * @version 1.10  2011/06/10 Iwata Naoki
 * @version 2.00  2011/11/17 Iwata Naoki(Yamagushiベース)
 * @version 2.10  2012/02/22 katayama (CAN,GPS,Encoderデータ取得追加)
 * @version 2.11  2016/05/13 katayama (CAN関数仕様変更)
 *
 * Copyright (C) 2010 - 2016 Sanritz Automation Co.,Ltd. All rights reserved.
 */

#ifndef TPLX_H_
#define TPLX_H_

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>

/************************************************************
 * マクロ定義
 ************************************************************/

#define HIGH                  (     1)  //!< @brief HIGH状態
#define LOW                   (     0)  //!< @brief LOW状態

#define PIN_DIGITAL_OUT_COUNT (     9)  //!< @brief デジタル出力ピン数
#define PIN_DIGITAL_IN_COUNT  (     9)  //!< @brief デジタル入力ピン数
#define PIN_PWM_COUNT         (    10)  //!< @brief PWM出力ピン数
#define PIN_ANALOG_IN_COUNT   (    10)  //!< @brief アナログ入力ピン数
#define PIN_PULSE_IN_COUNT    (     2)  //!< @brief パルス入力ピン数

#define PWM_MAX               (  1000)  //!< @brief PWM出力の最大値
#define PWM_MIN               ( -1000)  //!< @brief PWM出力の最小値
#define MOTOR_MAX             (  1000)  //!< @brief DCモータ出力の最大値
#define MOTOR_MIN             ( -1000)  //!< @brief DCモータ出力の最小値

#define PIN_DO1               (0x0000)  //!< @brief デジタル出力1ピン
#define PIN_DO2               (0x0001)  //!< @brief デジタル出力2ピン
#define PIN_DO3               (0x0002)  //!< @brief デジタル出力3ピン
#define PIN_DO4               (0x0003)  //!< @brief デジタル出力4ピン
#define PIN_DO5               (0x0004)  //!< @brief デジタル出力5ピン
#define PIN_DO6               (0x0005)  //!< @brief デジタル出力6ピン
#define PIN_DO7               (0x0006)  //!< @brief デジタル出力7ピン
#define PIN_DO8               (0x0007)  //!< @brief デジタル出力8ピン
#define PIN_DO9               (0x0008)  //!< @brief デジタル出力9ピン

#define PIN_DI1               (0x0000)  //!< @brief デジタル入力1ピン
#define PIN_DI2               (0x0001)  //!< @brief デジタル入力2ピン
#define PIN_DI3               (0x0002)  //!< @brief デジタル入力3ピン
#define PIN_DI4               (0x0003)  //!< @brief デジタル入力4ピン
#define PIN_DI5               (0x0004)  //!< @brief デジタル入力5ピン
#define PIN_DI6               (0x0005)  //!< @brief デジタル入力6ピン
#define PIN_DI7               (0x0006)  //!< @brief デジタル入力7ピン
#define PIN_DI8               (0x0007)  //!< @brief デジタル入力8ピン
#define PIN_DI9               (0x0008)  //!< @brief デジタル入力9ピン

#define PIN_PWM1              (0x0000)  //!< @brief PWM出力1ピン
#define PIN_PWM2              (0x0001)  //!< @brief PWM出力2ピン
#define PIN_PWM3              (0x0002)  //!< @brief PWM出力3ピン
#define PIN_PWM4              (0x0003)  //!< @brief PWM出力4ピン
#define PIN_PWM5              (0x0004)  //!< @brief PWM出力5ピン
#define PIN_PWM6              (0x0005)  //!< @brief PWM出力6ピン
#define PIN_PWM7              (0x0006)  //!< @brief PWM出力7ピン
#define PIN_PWM8              (0x0007)  //!< @brief PWM出力8ピン
#define PIN_PWM9              (0x0008)  //!< @brief PWM出力9ピン
#define PIN_PWM10             (0x0009)  //!< @brief PWM出力10ピン

#define PIN_ANALOGINPUT1      (0x0000)  //!< @brief アナログ入力1ピン
#define PIN_ANALOGINPUT2      (0x0001)  //!< @brief アナログ入力2ピン
#define PIN_ANALOGINPUT3      (0x0002)  //!< @brief アナログ入力3ピン
#define PIN_ANALOGINPUT4      (0x0003)  //!< @brief アナログ入力4ピン
#define PIN_ANALOGINPUT5      (0x0004)  //!< @brief アナログ入力5ピン
#define PIN_ANALOGINPUT6      (0x0005)  //!< @brief アナログ入力6ピン
#define PIN_ANALOGINPUT7      (0x0006)  //!< @brief アナログ入力7ピン
#define PIN_ANALOGINPUT8      (0x0007)  //!< @brief アナログ入力8ピン
#define PIN_ANALOGINPUT9      (0x0008)  //!< @brief アナログ入力9ピン
#define PIN_ANALOGINPUT10     (0x0009)  //!< @brief アナログ入力10ピン
#define PIN_ANALOGINPUT11     (0x000A)  //!< @brief アナログ入力11ピン
#define PIN_ANALOGINPUT12     (0x000B)  //!< @brief アナログ入力12ピン

#define PIN_POWER             (0x000C)  //!< @brief 電源電圧入力ピン

#define PIN_PULSEINPUT1       (0x0000)  //!< @brief パルス入力1ピン
#define PIN_PULSEINPUT2       (0x0001)  //!< @brief パルス入力2ピン

/** 制御有効ビット */
#define ENABLE_CTRL          (0x000001) //!< @brief 制御有効化
#define ENABLE_MOTOR         (0x000002) //!< @brief DCモータの制御を有効化
#define ENABLE_PWM1          (0x000004) //!< @brief PWM出力1ポートの制御を有効化
#define ENABLE_PWM2          (0x000008) //!< @brief PWM出力2ポートの制御を有効化
#define ENABLE_PWM3          (0x000010) //!< @brief PWM出力3ポートの制御を有効化
#define ENABLE_PWM4          (0x000020) //!< @brief PWM出力4ポートの制御を有効化
#define ENABLE_PWM5          (0x000040) //!< @brief PWM出力5ポートの制御を有効化
#define ENABLE_PWM6          (0x000080) //!< @brief PWM出力6ポートの制御を有効化
#define ENABLE_PWM7          (0x000100) //!< @brief PWM出力7ポートの制御を有効化
#define ENABLE_PWM8          (0x000200) //!< @brief PWM出力8ポートの制御を有効化
#define ENABLE_PWM9          (0x000400) //!< @brief PWM出力9ポートの制御を有効化
#define ENABLE_PWM10         (0x000800) //!< @brief PWM出力10ポートの制御を有効化
#define ENABLE_DO1           (0x001000) //!< @brief デジタル出力1ポートの制御を有効化
#define ENABLE_DO2           (0x002000) //!< @brief デジタル出力2ポートの制御を有効化
#define ENABLE_DO3           (0x004000) //!< @brief デジタル出力3ポートの制御を有効化
#define ENABLE_DO4           (0x008000) //!< @brief デジタル出力4ポートの制御を有効化
#define ENABLE_DO5           (0x010000) //!< @brief デジタル出力5ポートの制御を有効化
#define ENABLE_DO6           (0x020000) //!< @brief デジタル出力6ポートの制御を有効化
#define ENABLE_DO7           (0x040000) //!< @brief デジタル出力7ポートの制御を有効化
#define ENABLE_DO8           (0x080000) //!< @brief デジタル出力8ポートの制御を有効化
#define ENABLE_DO9           (0x100000) //!< @brief デジタル出力9ポートの制御を有効化

/** 全デジタル出力の制御を有効化 */
#define ENABLE_DO            (ENABLE_DO1|ENABLE_DO2|ENABLE_DO3|ENABLE_DO4|ENABLE_DO5|ENABLE_DO6|ENABLE_DO7|ENABLE_DO8|ENABLE_DO9)

/** 全PWM出力の制御を有効化 */
#define ENABLE_PWM           (ENABLE_PWM1|ENABLE_PWM2|ENABLE_PWM3|ENABLE_PWM4|ENABLE_PWM5|ENABLE_PWM6|ENABLE_PWM7|ENABLE_PWM8|ENABLE_PWM9|ENABLE_PWM10)

/** 全ポートの制御を有効化 */
#define ENABLE_ALL           (ENABLE_DO|ENABLE_MOTOR|ENABLE_PWM)


/************************************************************
 * 構造体定義
 ************************************************************/

/**
 * 制御情報を格納する構造体
 */
typedef struct TPIP_CTRL_STR {
	unsigned int   e_bit;      //!< @brief 制御有効ビット
	short          DC;         //!< @brief DCモータ出力値
	short          PWM[10];    //!< @brief PWM出力値(配列番号=ポート番号)
	union {
		unsigned short value;  //!< @brief デジタル出力値
		struct {
			unsigned port1:1;  //!< @brief デジタル出力1ポート
			unsigned port2:1;  //!< @brief デジタル出力2ポート
			unsigned port3:1;  //!< @brief デジタル出力3ポート
			unsigned port4:1;  //!< @brief デジタル出力4ポート
			unsigned port5:1;  //!< @brief デジタル出力5ポート
			unsigned port6:1;  //!< @brief デジタル出力6ポート
			unsigned port7:1;  //!< @brief デジタル出力7ポート
			unsigned port8:1;  //!< @brief デジタル出力8ポート
			unsigned port9:1;  //!< @brief デジタル出力9ポート
		}bit;
	}DO;

}tpip_ctrl;

/**
 * センサ情報を格納する構造体
 */
typedef struct TPIP_SENS_STR
{
	unsigned short nodata;
	unsigned short AI[12]; //!< @brief アナログ入力
	unsigned short PWR;    //!< @brief 電源電圧
	short          PI[2];  //!< @brief パルス入力
	unsigned short DI;     //!< @breif デジタル入力
}tpip_sens;


/**
 * CAN情報を格納する構造体
 */
typedef struct TPIP_CAN_STR
{
	unsigned char  flg;       // send/receive flag
	unsigned char  RTR;       // RTR
	unsigned char  sz;        // data size
	unsigned char  stat;      // status
	unsigned short STD_ID;    // standard ID
	unsigned char  data[8];   // data
}tpip_can;


/**
 * データ要求情報を格納する構造体
 */
typedef struct TPIP_RQ_STR
{
	unsigned char  ctrl;	// req size or EC ctrl flag(0:nop 1:reset)
	unsigned char  nodata;	// no user
}tpip_req;


/**
 * GPS データ情報を格納する構造体
 */
typedef struct TPIP_GPS_STR
{
	unsigned char  YY;			// UTC 日付(YY)
	unsigned char  MM;			// UTC 日付(MM)
	unsigned char  DD;			// UTC 日付(DD)
	unsigned char  reserv1;
	unsigned char  hh;			// UTC 時刻(hh)
	unsigned char  mm;			// UTC 時刻(mm)
	unsigned char  ss;			// UTC 時刻(ss)
	unsigned char  reserv2;
	int            la_deg;		// 緯度(latitude) [度] x1000000
	int            lo_deg;		// 経度(longitude)[度] x1000000
	unsigned char  GPS_qlty;	// GPS Quality <0:非測位 1:GSP測位 2:DGPS測位>
	unsigned char  sat_cnt;		// Satellite cont 衛星数
	unsigned short HDOP;		// HDOP(horizontal dilution of precision) x100
} tpip_gps;


/**
 * Encoder情報を格納する構造体
 */
typedef struct TPIP_EC_STR
{
	unsigned long  PI01_z;		// PI ch1  Z相
	long           PI01_ab;		// PI ch1 AB相
	unsigned long  PI02_z;		// PI ch2  Z相
	long           PI02_ab;		// PI ch2 AB相
	unsigned long  TMstamp;		// Time stamp
}tpip_encoder;



/************************************************************
 * 関数宣言
 ************************************************************/

/**
 * プロセス間通信処理タスク初期化関数<br>
 * TPLX_init関数はプロセス間通信処理タスクを初期化する関数です。
 * この関数を実行することで、TPIP Linux内のファームウェアと通信を開始し、
 * センサ情報の取得、制御情報の送信が可能になります。
 *
 * @retval == 0 : 正常終了
 * @retval <  0 : 異常終了
 *
 * @see TPLX_close
 *
 */
extern int TPLX_init();


/**
 * プロセス間通信処理タスク終了関数<br>
 * TPLX_close関数はプロセス間通信処理タスクを終了する関数です。
 * ユーザーはTPLX_init関数を実行した場合は、プログラムが終了する際にTPLX_close関数を必ず実行する必要があります。
 *
 * @retval >= 0 : 正常終了
 * @retval <  0 : 異常終了
 *
 */
extern int TPLX_close();


/**
 * TPLXバージョン取得関数
 * @retval > 0 バージョン情報
 */
extern int TPLX_version();


/**
 * 制御情報設定関数
 *
 * @param  data : 制御データ
 *
 * @retval == 0 : OK
 * @retval ==-1 : error
 *
 */
extern int TPLX_set_ctrl(tpip_ctrl* data);


/**
 * 制御情報有効化関数<br>
 * TPLX_set_enable関数はTPIP2内部から制御する制御情報を有効/無効にする関数です。
 * tpip_ctrl構造体のe_bitを変更し、TPLX_set_ctrl関数で設定するのと同等の操作を行うことが出来ます。
 *
 * @param value : 制御有効ビット
 *
 * @retval <  0 : 異常終了
 * @retval >= 0 : 正常終了。以前設定されていた制御有効ビットの値。
 *
 */
//extern int TPLX_set_enable(unsigned int value);


/**
 * PWM出力設定関数
 *
 * @param ch    : 出力値を設定するPWMポートの番号
 * @param value : 出力値（PWM_MIN～PWM_MAX）
 *
 * @retval <  0 : 異常終了
 * @retval >= 0 : 正常終了。以前設定されていた出力値の値。
 *
 */
extern int TPLX_set_pwm(unsigned int ch, short value);


/**
 * デジタル出力設定関数
 *
 * @param ch    : 出力値を設定するデジタル出力ポートの番号
 * @param value : 出力値（HIGH / LOW）
 *
 * @retval <  0 : 異常終了
 * @retval >= 0 : 正常終了。以前設定されていた出力値の値。
 *
 */
extern int TPLX_set_do(unsigned int ch, unsigned short value);

/**
 * モーター出力設定関数
 *
 * @param value : 出力値(MOTOR_MIN～MOTOR_MAX)
 *
 * @retval <  0 : 異常終了
 * @retval >= 0 : 正常終了。以前設定されていた出力値の値
 *
 */
extern int TPLX_set_motor(short value);


/**
 * センサー情報取出関数<br>
 * TPLX_get_sens関数はセンサ情報を取得する関数です。
 *
 * @param  data : センサ情報構造体のポインタ
 *
 * @retval <  0 : 異常終了
 * @retval == 0 : 正常終了
 *
 */
extern int TPLX_get_sens_ex(tpip_sens* data, int wait_time);
extern int TPLX_get_sens(tpip_sens* data);


/**
 * アナログ入力情報取得関数<br>
 * TPLX_get_analog関数はアナログ入力の入力値を取得する関数です。
 *
 * @param ch    : 入力値を取得するアナログ入力のポート番号
 * @param value : 入力値を格納する変数のポインタ
 *
 * @retval <  0 : 異常終了
 * @retval == 0 : 正常終了
 *
 */
extern int TPLX_get_analog(unsigned int ch, unsigned short* value);


/**
 * デジタル入力情報取得関数<br>
 *
 * @param ch    : 入力値を取得するデジタル入力のポート番号
 * @param value : 入力値を格納する変数のポインタ
 *
 * @retval <  0 : 異常終了
 * @retval == 0 : 正常終了
 */
extern int TPLX_get_di(unsigned int ch, unsigned short* value);


/**
 * パルス入力情報取得関数<br>
 *
 * @param ch    : 入力値を取得するパルス入力のポート番号
 * @param value : 入力値を格納する変数のポインタ
 *
 * @retval <  0 : 異常終了
 * @retval == 0 : 正常終了
 */
extern int TPLX_get_pulse(unsigned int ch, short* value);


/**
 * 9512応答方向 切替関数<br>
 *
 * @param dir  : 応答方向(0= 9512->JTCP, 1= 9512->Proc)
 *
 * @retval <  0 : 異常終了
 * @retval == 0 : 正常終了
 */
extern int TPLX_set_dir(unsigned char dir);


/**
 * CANデータ送信関数
 * @fn     int TPLX_send_can( tpip_can* data )
 * @param  data : 送信データ(CAN header + data)
 * @retval == 0 : OK
 * @retval ==-1 : error
 */
extern int TPLX_send_can( tpip_can* data );


/**
 * CAN　キャッシュ・データの取得関数
 * @fn     int TPLX_find_can(unsigned short ID, tpip_can* recv_buf )
 * @param  ID       : 検索するSTD_ID
 * @param  recv_buf : 受信データ(CAN header + data)
 * @retval == 0     : OK
 * @retval ==-1     : error
 */
extern int TPLX_find_can(unsigned short ID, tpip_can* recv_buf );


/**
 * Remoteフレーム送信 & Dataフレーム受信
 * @fn     int TPLX_get_can( tpip_can* recv_buf, int wait_tm )
 * @param  recv_buf : 受信データ(CAN header + data)
 * @param  wait_tm  : 受信待ち時間(ms)
 * @retval == 0     : OK
 * @retval ==-1     : error
 */
extern int TPLX_get_can( tpip_can* recv_buf, int wait_tm );

/**
 * GPSデータ取得関数
 *
 * @param  data    : GPS情報格納エリア
 * @param  wait_tm : 待ち時間(N ms)
 * @param  reset   : 0= normal 
 *
 * @retval <  0 : 異常終了
 * @retval == 0 : 正常終了
 */
extern int TPLX_get_gps( tpip_gps* data, int wait_tm );

/**
 * Encode情報取得関数<br>
 *
 * @param data    : Encoder データ格納バッファのポインタ
 * @param wait_tm : 待ち時間(ms)
 * @param reset   : 0=通常, 1=Reset Encode counter
 *
 * @retval <  0 : 異常終了
 * @retval == 0 : 正常終了
 */
extern int TPLX_get_encoder( tpip_encoder* data, int wait_tm , int reset );

#endif /* TPLX_H_ */
