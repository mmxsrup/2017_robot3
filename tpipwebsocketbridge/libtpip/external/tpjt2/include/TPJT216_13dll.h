/** 
* TPIP2 JTP communication(16bit) Header file
 * @file TPJT216_10dll.h
 * @brief JTP communication プログラム　DLL ヘッダーファイル
 *
 * @author Katayama
 * @date 2003-03-20
 * @version $Id: TPJT_10.dll   ,v 1.0  2008/03/20 00:00:00 katayama $
 * @version $Id: TPJT_11.dll   ,v 1.1  2008/05/04 00:00:00 katayama $
 * @version $Id: TPJT_11.dll   ,v 1.1a 2008/05/15 00:00:00 katayama $
 * @version $Id: TPJT_11.dll   ,v 1.1b 2008/07/24 00:00:00 katayama $
 * @version $Id: TPJT16_11.dll ,v 1.1c 2008/09/21 00:00:00 katayama $
 * @version $Id: TPJT216_11.dll ,v 2.00  2008/10/08 00:00:00 katayama $
 * @version $Id: TPJT216_12.dll ,v 2.10  2009/03/31 00:00:00 katayama $
 * @version $Id: TPJT216_13.dll ,v 2.20  2009/06/08 00:00:00 katayama $
 * @version $Id: TPJT216_13.dll ,v 2.21  2009/06/29 00:00:00 katayama $
 *
 * Copyright (C) 2008 TPIP User Community All rights reserved.
 * このファイルの著作権は、TPIPユーザーコミュニティの規約に従い
 * 使用許諾をします。
 */

/** \mainpage
 * このモジュールは動画伝送の制御プロトコル処理です。
 */
#ifndef _TPJT216_13DLL_H__
#define _TPJT216_13DLL_H__

#pragma pack(1)

struct OUT_DT_STR {
	unsigned short d_out;	// [ver 2.21]DO bit8 - 0
	short          MT;
	short          PWM[10];
};

struct INP_DT_STR {
	unsigned short b_ptn;	// [ver 2.20]
	unsigned short AI[12];	// [ver 2.10]
	unsigned short batt;	// [ver 2.20]
	short          PI1;		// [ver 2.20]
	short          PI2;		// [ver 2.20]
	unsigned short DI;
};

//
// CAN data 
typedef struct mctrl_can_str{	// Ver 2.00a
	unsigned char  flg;		// send/receive flag
	unsigned char  RTR;		// RTR
	unsigned char  sz;		// data size
	unsigned char  stat;	// status
	unsigned short STD_ID;	// standard ID
	unsigned char  data[8];	// data
} mctrl_can_t ;


#pragma pack(8)

extern void  __stdcall TPJT_init(char* host_ip, HWND hwnd);
extern void  __stdcall TPJT_close(void);
extern void  __stdcall TPJT_set_ctrl(void* buf, int sz);
extern void  __stdcall TPJT_get_sens(void* buf, int buf_sz);
extern void  __stdcall TPJT_chg_camera(int no);
extern int   __stdcall TPJT_set_rSIO_para(int UDP_port, int SIO_speed);	// [Ver 1.1] append
extern unsigned long __stdcall TPJT_JF_get_stamp(void);					// [Ver 1.1b] append
extern int   __stdcall TPJT_Get_Wlink(void);							// [Ver 1.1b] append
extern void  __stdcall TPJT_Set_sync(HWND hwnd);						// [Ver 1.1b] append
extern int   __stdcall TPJT_Get_stat(void);							// [Ver 2.00] append
extern int   __stdcall TPJT_Send_CANdata(mctrl_can_t* buf, int sz);	// [Ver 2.00a] append
extern int   __stdcall TPJT_Recv_CANdata(mctrl_can_t* buf);			// [Ver 2.00a] append
extern void  __stdcall TPJT_init_ex(char* host_ip, HWND hwnd, int req);	// [Ver 2.10] append

extern int   __stdcall TPJT_get_jpeg(void* buf, int buf_sz);
extern int   __stdcall TPJT_get_vspeed(void);

extern int   __stdcall TPJT_set_com_req(int req, int w_time);
extern int   __stdcall TPJT_get_com_req(void);
extern int   __stdcall TPJT_get_com_stat(void);
extern int   __stdcall TPJT_get_com_ver(void);
extern void  __stdcall TPJT_set_video_inf(int type);
extern void  __stdcall TPJT_set_vspeed(int speed);

extern void* __stdcall TPJT_get_jpeg_file(int wait_flg, int wait_time, int* dwBytesRead);
extern void  __stdcall TPJT_free_jpeg_file(void);

extern int   __stdcall TPJT_Get_dtime(void);		// [Ver 1.1b] append
extern int   __stdcall TPJT_Get_Staytime(void);		// [Ver 1.1b] append

extern void __stdcall TPJT_Set_auto(int val);		// [Ver 1.1c] append
extern int  __stdcall TPJT_get_com_mode(void);		// [Ver 2.10] append


#endif /* ifndef _TPJT16_11DLL_H__ */
