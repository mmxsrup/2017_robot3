﻿cmake_minimum_required( VERSION 2.8 )

# ~~~~ set comiler for specified TPIP ~~~~ #
set( TARGET "TPJT3" CACHE STRING "target (TPJT3(default) or TPJT2 or TPLX3 or TPLX2 or TPIP_EMULATOR)")
if( "${TARGET}" STREQUAL "TPLX3")
  set(CMAKE_TOOLCHAIN_FILE "${CMAKE_CURRENT_LIST_DIR}/libtpip/cross-tpip3.cmake")
elseif("${TARGET}" STREQUAL "TPLX2")
  set(CMAKE_CXX_FLAGS "-Wall")
  set(CMAKE_TOOLCHAIN_FILE "${CMAKE_CURRENT_LIST_DIR}/libtpip/cross-tpip2.cmake")
endif()

set (CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_FLAGS "-Wall -std=gnu++11")
set(CMAKE_CXX_FLAGS_DEBUG "-g")
set(CMAKE_CXX_FLAGS_RELEASE "-O2")

set(CMAKE_C_FLAGS "-Wall -std=gnu99")
set(CMAKE_C_FLAGS_DEBUG "-g")
set(CMAKE_C_FLAGS_RELEASE "-O2")

project( TpipWebsocketBridgeFramework )
set( ${PROJECT_NAME}_VERSION_MAJOR 0 )
set( ${PROJECT_NAME}_VERSION_MINOR 1 )

# ~~~~ libwebsockets ~~~~ #
set( LWS_WITH_SHARED        OFF CACHE BOOL "")
set( LWS_WITH_SSL           OFF CACHE BOOL "")
set( LWS_WITH_ZLIB          OFF CACHE BOOL "")
set( LWS_WITHOUT_CLIENT     ON  CACHE BOOL "")
set( LWS_WITHOUT_TESTAPPS   ON  CACHE BOOL "")
set( LWS_WITHOUT_EXTENSIONS ON  CACHE BOOL "")
add_subdirectory( libwebsockets )
include_directories( ${PROJECT_SOURCE_DIR}/libwebsockets/lib/ )
include_directories( ${PROJECT_BINARY_DIR}/libwebsockets/ )

# ~~~~ jansson (submodule) ~~~~ #
set( JANSSON_BUILD_DOCS    OFF CACHE BOOL "")
set( JANSSON_EXAMPLES      OFF CACHE BOOL "")
set( JANSSON_WITHOUT_TESTS ON  CACHE BOOL "")

add_subdirectory( jansson )
include_directories( ${PROJECT_SOURCE_DIR}/jansson/lib/ )
include_directories( ${PROJECT_BINARY_DIR}/jansson/include /)

# ~~~~ TPIP ~~~~ #

add_subdirectory( libtpip )
include_directories( ${PROJECT_SOURCE_DIR}/libtpip/src )
include_directories( ${PROJECT_BINARY_DIR}/libtpip )

# ~~~~ main ~~~~ #

file(GLOB_RECURSE headers src/*.h src/*.hpp)
file(GLOB_RECURSE sources src/*.c src/*.cpp)

message("detected source and headers : ${headers} ${sources}")

include_directories( ${PROJECT_BINARY_DIR}/ )
include_directories( ${PROJECT_SOURCE_DIR}/src )
include_directories( ${PROJECT_SOURCE_DIR}/include )

### ### Executable library ### ###

string(TOLOWER ${TARGET} TARGET_LOWER)
set(EXECUTABLE_LIB_NAME "${PROJECT_NAME}_${TARGET_LOWER}")
add_library( ${EXECUTABLE_LIB_NAME} ${sources} ${headers})
add_dependencies( ${EXECUTABLE_LIB_NAME} websockets jansson tpip ${TPIP_LIBS})
target_link_libraries( ${EXECUTABLE_LIB_NAME} websockets jansson tpip ${TPIP_LIBS})

# TODO : 複数ファイルコピーするもっと良い表記をする..
configure_file("${PROJECT_SOURCE_DIR}/libwebsockets/lib/libwebsockets.h"
  "${PROJECT_BINARY_DIR}" COPYONLY)
configure_file("${PROJECT_BINARY_DIR}/jansson/include/jansson.h"
  "${PROJECT_BINARY_DIR}" COPYONLY)
configure_file("${PROJECT_BINARY_DIR}/jansson/include/jansson_config.h"
  "${PROJECT_BINARY_DIR}" COPYONLY)
configure_file("${PROJECT_BINARY_DIR}/libwebsockets/lws_config.h"
  "${PROJECT_BINARY_DIR}" COPYONLY)
configure_file("${PROJECT_SOURCE_DIR}/libtpip/src/tpip.h"
  "${PROJECT_BINARY_DIR}" COPYONLY)
configure_file("${PROJECT_SOURCE_DIR}/libtpip/src/tpip_hardware.h"
  "${PROJECT_BINARY_DIR}" COPYONLY)
configure_file("${PROJECT_BINARY_DIR}/libtpip/tpip_config.h"
  "${PROJECT_BINARY_DIR}" COPYONLY)


### for webbrower client ###

set(SERVER_RESOURCE_DIR ${PROJECT_SOURCE_DIR}/client/webbrowser)

configure_file("${PROJECT_SOURCE_DIR}/config.h.in"
               "${PROJECT_BINARY_DIR}/config.h")

option(WITH_SAMPLE "compile with sample program" ON)
if(${WITH_SAMPLE})
  #  set(SAMPLE "${EXECUTABLE_LIB_NAME}_sample")
  #  file(GLOB_RECURSE sample_headers sample/*.h sample/*.hpp)
  #  file(GLOB_RECURSE sample_sources sample/*.c sample/*.cpp)
  #  add_executable(${SAMPLE} ${sample_headers} ${sample_sources})
  #  target_link_libraries(${SAMPLE} ${EXECUTABLE_LIB_NAME})
  #
  #  set(SAMPLE2 "${EXECUTABLE_LIB_NAME}_sample2")
  #  file(GLOB_RECURSE sample2_headers sample2/*.h sample2/*.hpp)
  #  file(GLOB_RECURSE sample2_sources sample2/*.c sample2/*.cpp)
  #  add_executable(${SAMPLE2} ${sample2_headers} ${sample2_sources})
  #  target_link_libraries(${SAMPLE2} ${EXECUTABLE_LIB_NAME})

  set(SERVER_SRC "${EXECUTABLE_LIB_NAME}_server_src")
  file(GLOB_RECURSE server_src_headers server_src/*.h server_src/*.hpp)
  file(GLOB_RECURSE server_src_sources server_src/*.c server_src/*.cpp)
  add_executable(${SERVER_SRC} ${server_src_headers} ${server_src_sources})
  target_link_libraries(${SERVER_SRC} ${EXECUTABLE_LIB_NAME})
endif()

