[ホーム](readme.md) > コンパイル

# コンパイル

以下はサンプルプログラムのコンパイル方法

各ロボット用のプログラムの製作方法は[このページ](readme.customize.md)参照

ポイントだけ列挙すると

- cmake使っている
- cmakeするときに適切に`TARGET`を指定する



## Linux, Mac, Windows(CUI)

git, make, cmake, c,c++コンパイラなどが必要

windowsは開発者用コマンドプロンプトで行う

~~~
git clone --recursive https://bitbucket.org/cormoran707/tpipwebsocketbridge
mkdir build
cd build
cmake .. -DTARGET=TPIP_EMULATOR
# 以下、各ターゲットごとの例
# cmake .. -DTARGET=TPJT2
# cmake .. -DTARGET=TPJT3
# cmake .. -DTARGET=TPLX2
# cmake .. -DTARGET=TPLX3
# TPIP_EMULATOR指定時 -DTPIP_EMULATOR_USE_OPENCV=OFF をつけると
# opencvなしモードで使えるはず（映像ストリーミングは機能しなくなる）
# TPJT2,3 は windows only , TPLXは未実装だったりする

#unix
make
./TpipWebsockBridge-...

#window (動かないという噂を聞いた)
msbuild
.\Debug\TpipWebsockBridge-...

# 実行ファイル名のサフィックスはTARGETによって変わるので適当に
~~~

## Windows(GUI)


+ SourceTree 等で https://bitbucket.org/cormoran707/tpipwebsocketbridge をclone
+ cmake Gui でconfigure,generate (TARGETを適切に設定する)
+ VisualStudioでプロジェクトを開いてコンパイル、実行
    + デフォルトでは実行のターゲットみたいなものがALLになっているので変更する必要がある
    + cmakeでVisualStudio使う例を調べると多分色々出てくる