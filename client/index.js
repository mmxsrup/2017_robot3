/* ? ms ごとに呼び出す関数を登録 */

/*
 * SendToServerforWd4() := wd4 オブジェクトの値をサーバーに送る
 * SendToServerforBeltConveyor() := BeltConveyor オブジェクトの値をサーバーに送る
 * UpdateDisplay() := state と wd4 オブジェクトの値の一部を クライアントに表示
 * checkGamepads() := gamepadの値を読み取る
 */


$(document).ready(function() {
	setInterval("SendToServerformc()", 100);
	setInterval("SendToServerforbeltH()", 100);
	setInterval("SendToServerforbeltV()", 100);
	setInterval("UpdateDisplay()", 100);
	setInterval("checkGamepads()", 100);
})