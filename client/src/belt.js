/*
"belt_hori" : {
    "go" : 0, (-255~+255)
    "roll" : 0, (-255~+255)
}

"belt_vert" : {
    "up" : 0, (-255~+255)
}
*/

var beltH = {
    "belt_hori" : {
        "go" : 0,
        "roll" : 0,
    },
}
var beltV = {
    "belt_vert" : {
        "up" : 0,
    },
}


$("#bc_btnGo").click(function() {
	console.log("btnGo");
    var val = parseInt($("#bc_Go").val(), 10);

    beltH.belt_hori.go = val;
    ws.send(JSON.stringify(
        beltH,
    )); 
})

$("#bc_btnRoll").click(function() {
    var val = parseInt($("#bc_Roll").val(), 10);
    beltH.belt_hori.roll = val;
    ws.send(JSON.stringify(
        beltH,
    )); 
})

$("#bc_btnUp").click(function() {
	var val = parseInt($("#bc_Up").val(), 10);
    beltV.belt_vert.up = val;
    ws.send(JSON.stringify(
        beltV,
    )); 
})


