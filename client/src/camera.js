var camera_no = 0;
var camera_total_number = 2;
var tpip_request_speed = 200;

var ws_camera_stream = new WebSocket(SERVER_URL, "camera-stream");


var objecturl;
var img = new Image;
var img_pre_w = 0,
    img_pre_h = 0;

ws_camera_stream.onmessage = function(evt) {
    var data = evt.data;
    if (typeof data === 'string') { // 設定が飛んできた
        var data = JSON.parse(data);
        console.warn(data);
        if ("ip" in data) {
            $("#ip").html(data["ip"]);
        }
        if ("camera" in data) {
            $("#camera_no").html(data["camera"]);
        }
        if ("kbps" in data) {
            $("#com_speed").html(data["kbps"]);
        }
        if ("status" in data) {
            $("#com_stat").html(data["status"]);
        }
    } else { // バイナリ(jpegデータ)が飛んできた

        console.warn("jpeg", window, window.URL);
        // データからobjectURL を作って、それをimgのsrcにすると画像が表示される（簡単！！）
        window.URL.revokeObjectURL(objecturl);
        objecturl = URL.createObjectURL(data);
        img.src = objecturl;
        console.warn("jpeg1");
        img.onload = function() {
            console.warn("jpeg2");
            var canvas = $("#img")[0];
            var c2d = canvas.getContext("2d");
            if (img_pre_h != img.height || img_pre_w != img.width) { // 画像サイズが変わった時のキャンバスサイズ変更
                c2d.clearRect(0, 0, canvas.width, canvas.height);
                img_pre_h = img.height;
                img_pre_w = img.width;
            }
            c2d.drawImage(img, 0, 0, img.width, img.height);
        };
    }
};

// 500msごとに設定データを要求する（最新の設定データがいらないなら不要）
setInterval(function() {
    ws_camera_stream.send(JSON.stringify({
        get: true
    }));
}, 500);



function changeCamera() {
    camera_no = (camera_no + 1) % camera_total_number;
    console.warn(camera_no);
    ws_camera_stream.send(JSON.stringify({
        camera: camera_no,
        get: true,
    }));
}
$("#camera_change").click(function() {
    console.warn("click");
    changeCamera();
})
