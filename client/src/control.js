var ws = new WebSocket(SERVER_URL, "control");



function SendToServerformc() {
    console.log("send", mc);
    ws.send(JSON.stringify(
        mc,
    ));
}
function SendToServerforbeltH() {
    console.log("send", beltH);
    ws.send(JSON.stringify(
        beltH,
    ));
}
function SendToServerforbeltV() {
    console.log("send", beltV);
    ws.send(JSON.stringify(
        beltV,
    ));
}

/*
 * コントローラーの Axis の値 をパースして, オブジェクトの値を変える関数
 * arg := Axisの値は入ったオブジェクト
 */
function ParseDataAxes(arg) {
    if(stopflag) return true;
    // console.warn("ParseDataAxes", arg.length);
    var mc_dir_y = 0.0, mc_dir_x = 0.0;
    var mc_speed_go = 0.0;
    for (var index = 0; index < arg.length; index++) {
        var data = arg[index];
        switch (index) {
            case AXIS_MC_GO_DIR_Y : 
                mc_dir_y = data;
                mc_speed_go += data * data;
                break;
            case AXIS_MC_GO_DIR_X : 
                mc_dir_x = data;
                mc_speed_go += data * data;
                break;
        }
    }
    var rad = Math.atan2(mc_dir_y, mc_dir_x);
    var deg = rad * 180 / Math.PI;
    var ret = Math.ceil(deg);
    // console.warn(ret, deg, rad, mc_dir_y, mc_dir_x);
    mc = mcGo;
    // mcDisplay.mecanum.go = mc.mecanum.go;
    mc.mecanum.go.dir    = ret;
    // mc_speed_goが[0.0, 1.0]なので, 範囲を[0 ~ 255]に正規化
    mc.mecanum.go.speed = Math.ceil(mc_speed_go * 255.0 / 2.0);
    console.warn(mc.mecanum.go.dir, mc.mecanum.go.speed);
}   

/*
 * コントローラの Button の値 をパースして, オブジェクトんも値を変える関数
 * arg := true ならボタンは押されていて, falseなら押されていない
 * index := 何番目のindexのボタンか
 */
function ParseDataButton(arg, index) {
    // console.log("ParseDataButton");
    if(!arg) return; /* ボタンが押されていない場合 */
    switch (index) {
        /* 指定されたボタンに応じて*/
        case BUTTON_BC_GO_FORWARD : 
            beltH.belt_hori.go = BC_SPEED_GO;
            break;
        case BUTTON_BC_GO_BACK :
            beltH.belt_hori.go = -BC_SPEED_GO
            break;
        case BUTTON_BC_GO_STOP :
            beltH.belt_hori.go = 0;
            break;
        case BUTTON_BC_ROLL : 
            beltH.belt_hori.roll = BC_SPEED_ROLL;
            break;
        case BUTTON_BC_ROLL_RE : 
            beltH.belt_hori.roll = -BC_SPEED_ROLL;
            break;
        case BUTTON_BC_ROLL_STOP :
            beltH.belt_hori.roll = 0;
            break;
        case BUTTON_BC_UP :
            beltV.belt_vert.up = BC_SPEED_UP;
            break;
        case BUTTON_BC_DOWN :
            beltV.belt_vert.up = -BC_SPEED_UP;
            break;
        case BUTTON_BC_UP_STOP :
            beltV.belt_vert.up = 0;
            break;
        case BUTTON_MC_ROLL_RIGHT :
            mc = mcRoll;
            mc.mecanum.roll.dir   = 1;
            mc.mecanum.roll.speed = MC_SPEED_ROLL;
            break;
        case BUTTON_MC_ROLL_LEFT :
            mc = mcRoll;
            mc.mecanum.roll.dir   = -1;
            mc.mecanum.roll.speed = MC_SPEED_ROLL;
            break;
        case BUTTON_MOVE_STOP :
            console.warn("STOP");
            if(stopflag) {
                stopflag = false;
            }else {
                mc = mcStop;
                mc.mecanum.stop = true;
                stopflag = true;
            }
            break;
    }
}