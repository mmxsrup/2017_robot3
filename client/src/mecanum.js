

/* 定期的に送るjsonオブジェクトを mcに入れておく */
var mc = {

}
var mcStop = {
    "mecanum" : {
        "stop" : true,
    },
}
var mcFree = {
    "mecanum" : {
        "free" : true,
    },
}
var mcRoll = {
    "mecanum" : {
        "roll" : {
            "dir" : 0,
            "speed" : 0,
        },
    },
}
var mcGo = {
    "mecanum" : {
        "go" : {
            "dir" : 0,
            "speed" : 0,
        },
    },
}

/*
 * クライアントのボタンから control.js に宣言されている 
 * state と wd4 オブジェクトの値 を変更してサーバーに投げる
 */


$(document).ready(function() {

    $("#mc_btnstop").click(function() {
        console.log("mecanum stop");
        mc = mcStop;
        mc.mecanum.stop = true;
        // mcDisplay.mecanum.stop = mc.mecanum.stop;
        ws.send(JSON.stringify(
            mc
        )); 
    })
    $("#mc_btnfree").click(function() {
        console.log("mecanum free");
        mc = mcFree;
        mc.mecanum.free = true;
        // mcDisplay.mecanum.free = mc.mecanum.free;
        ws.send(JSON.stringify(
            mc
        )); 
    })
    $("#mc_btnroll").click(function() {
        var dir = parseInt($("#mc_textrolldir").val(), 10);
        var speed = parseInt($("#mc_textrollspeed").val(), 10);
        mc = mcRoll;
        mc.mecanum.roll.dir = dir;
        mc.mecanum.roll.speed = speed;
        // mcDisplay.mecanum.roll = mc.mecanum.roll;
        console.log("mcroll", dir, speed);
        ws.send(JSON.stringify(
            mc
        ));
    })
    $("#mc_btngo").click(function() {
        var dir = parseInt($("#mc_textgodir").val(), 10);
        var speed = parseInt($("#mc_textgospeed").val(), 10);
        mc = mcGo;
        mc.mecanum.go.dir = dir;
        mc.mecanum.go.speed = speed;
        // mcDisplay.mecanum.go = mc.mecanum.go;
        console.log("mcgo", dir, speed);
        ws.send(JSON.stringify(
            mc
        ));
    })
    
})  