/* サーバー(pc) のipアドレスを設定 */
var SERVER_IP = "192.168.21.153:8080";
var SERVER_URL = "ws://" + SERVER_IP;


/*
 * 以下の設定について
 * Gamepad Tester (http://html5gamepad.com/)
 * 上記のサイトを利用し, AxisやButtonのindexどこに割り当てられているかを登録 
 * OSやブラウザの環境依存が激しいためここで設定
 */

var GAMEPAD_NAME = "";

/*
 * AXIS_MC_GO_DIR_Y  := 進む方向を決めるAXISのY方向
 * AXIS_MC_GO_DIR_X  := 進む方向を決めるAXISのX方向
 */
var AXIS_MC_GO_DIR_Y = 3;
var AXIS_MC_GO_DIR_X = 2;

/*
 * joystickを前方へ倒した場合にGamepad Tester(http://html5gamepad.com/) が -1になる時 false
 * そうでなければ true
 */
var MC_FLAG = false;


/*
 * カメラアームを動かすための十字keyボタンの登録 
 * (サーボ自体は絶対座標で制御が, R(右), L(左), U(上), D(下) で今の位置よりどちらがわに動かすかを操作できるようにする)
 * BUTTON_BC_GO_FORWARD := ベルトコンベアを前へ
 * BUTTON_BC_GO_BACK    := ベルトコンベアを後ろへ
 * BUTTON_BC_GO_STOP    := ベルトコンベアストップ
 * BUTTON_BC_ROLL       := ベルトを巻き込む
 * BUTTON_BC_ROLL_STOP  := ベルトの巻き込みを停止
 */
var BUTTON_BC_GO_FORWARD   = 12;
var BUTTON_BC_GO_BACK      = 13;
var BUTTON_BC_GO_STOP      = 15;
var BUTTON_BC_ROLL         = 6;
var BUTTON_BC_ROLL_RE      = 7;
var BUTTON_BC_ROLL_STOP    = 4;
var BUTTON_BC_UP           = 0;
var BUTTON_BC_DOWN         = 2;
var BUTTON_BC_UP_STOP      = 1;

/*
 * ベッドのgoとroll のスピードは-255~255で変更可能だが, 必要なさそうなので定数で固定
 */
var BC_SPEED_GO   = 100;
var BC_SPEED_ROLL = 100;
var BC_SPEED_UP   = 100;


/*
 * BUTTON_MC_ROLL_R   := その場右回転の向きを割り当てるボタン
 * BUTTON_MC_ROLL_L   := その場左回転のスピードを割り当てるボタン
 */
var BUTTON_MC_ROLL_RIGHT = 8;
var	BUTTON_MC_ROLL_LEFT  = 9;

/*
 * 足回りSTOP 
 * STOP ボタンを押すと stopflagがトグルされ, 
 */
var BUTTON_MOVE_STOP = 11;
var stopflag = true;


/*
 * メカナムのその場回転のスピードは変更可能だが, 必要なさそうなので定数で固定
 */
var MC_SPEED_ROLL = 100;


var op = {
	"メカナムの前後左右" : "右Axis(縦持ち)",
	"メカナム停止" : "右Axis押し込み (右Axisからの命令が停止)",
	"右回転" : "START",
	"左回転" : "SELECT",
	"ベルト前" : "+字 上",
	"ベルト後" : "+字 下",
	"ベルト前後STOP" : "+字 右",
	"ベルトロール" : "L1",
	"ベルト逆ロール" : "R1",
	"ベルトロールSTOP" : "L2",
	"ベルトUP" : "△", 
	"ベルトDOWN" : "✖︎",
	"ベルトUPSTOP" : "◯",
}
