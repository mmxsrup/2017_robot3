/*
 * http://mzsm.me/2015/02/14/orekike11-gamepadapi/ ここ参考
 */
function checkGamepads(){
    var gamepadList = navigator.getGamepads();
    for(var i=0; i<gamepadList.length; i++){
        var gamepad = gamepadList[i];
        // ブラウザによっては配列にundefinedが入っている場合がある
        if(gamepad){
            // Gamepad#id string ゲームパッドのデバイス名等
            // console.log(gamepad.id)
            // => "Xbox 360 Controller (XInput STANDARD GAMEPAD)"
            if (gamepad.id != GAMEPAD_NAME) continue;
            // Gamepad#mapping string キーマップピングが標準かどうか
            // ("standard"…標準 ""(空文字列)…独自)
            // console.log(gamepad.mapping);
            // => "standard"
 
            // Gamepad#axes double[] 方向キーの入力方向と強さの配列
            // (0が真ん中、-1に近づくと上/左方向、1に近づくと下/右方向)
            // キーに触ってないときもちょうど0になるわけではなく0.03くらいの値になるので注意
            // console.log(gamepad.axes);
            // => [
            //   -0.9935354739878914,
            //   0.03535467849054890,
            //   -0.5789490786325657,
            //   0.6474398798231042  
            // ]

            // Gamepad#buttons GamepadButton[] -> ボタンオブジェクトの配列
            // console.log("button len ", gamepad.buttons.length);
            for(var i=0; i<gamepad.buttons.length; i++){
                var button = gamepad.buttons[i];
                // GamepadButton#pressed bool ボタンが押されているかどうか
                // console.log(button.pressed);
                // => true;
                ParseDataButton(button.pressed, i);

                // GamepadButton#value double (アナログ入力の場合)押されている強さ(0～1)
                // アナログ入力でない場合は0/1のみ
                // console.log(button.value);
                // => 0.7893214690458746648
            }
            ParseDataAxes(gamepad.axes);
        }
    }
    // window.requestAnimationFrame(checkGamepads);
}
// ボタンが押されたり、方向キーの入力強さが変わったりしてもイベントは発生しないので、
// requestAnimationFrameを使い、ブラウザの描画更新タイミングごとに値をチェックする
// window.requestAnimationFrame(checkGamepads);
